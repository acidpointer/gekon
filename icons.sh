#!/usr/bin/env bash

SCRIPT="$(readlink -f "${BASH_SOURCE[0]}")"
DIR="$(dirname "$SCRIPT")"

if [[ -f "$DIR/icons.json" ]]; then
    cp "$DIR/icons.json" "$DIR/app/adminui/src"
    cp "$DIR/icons.json" "$DIR/app/server/src"
fi