/*
 * Terminal thrift schema v1.0
 * Author: acidpointer (acidpointer@gmail.com)
 *
 *
 */

struct SVD {
  1: i32 id,

  2: i32 err_chtn,
  3: i32 err_gvs,
  4: i32 err_connect_snd,
  5: i32 err_connect_terminal,

  6: i32 timer_5sec_start,
  7: i32 timer_180sec_stop,
  8: i32 timer_180sec_start,

  9: i32 chtn,
  
  10: i32 block1,
  11: i32 block2,
  12: i32 block3,
  13: i32 block4,

  14: double dnd1,

  15: double temp_gvs,
  16: double temp_pvd,

  17: i32 ekn1,
  18: i32 ekn2,
  19: i32 ekn3,

  20: i32 ekpn,
  21: i32 ekvk,

  22: i32 dozpn,
  23: i32 dozvk,

  24: i32 ekgvs
  25: i32 mode,
}

struct SND {
  1: i32 id,

  2: double dnd1,
  3: double dnd2,
  4: double dnd3,
  5: double dnd4,

  6: double du1,
  7: double du2,
  8: double du3,
  9: double du4,

  10: double ddk,
  11: double temp,

  12: i32 fc_err1,
  13: i32 fc_err2,
  14: i32 fc_err3,

  15: i32 block1,
  16: i32 block2,
  17: i32 block3,
  18: i32 block4,

  19: i32 block11,
  20: i32 block22,
  21: i32 block33,
  22: i32 block44,

  23: i32 n1_timer,
  24: i32 n2_timer
  25: i32 n3_timer,
  26: i32 n4_timer,

  27: i32 millis1,
  28: i32 millis2,
  29: i32 millis3,
  30: i32 millis4,

  31: i32 ekv,
}

enum TerminalAction {
  NONE            = 0, // We do nothing
  STOP            = 1, // Stop terminal
  START           = 2, // Start terminal
  RESTART         = 3, // Restart terminal
  RESTART_OS      = 4, // Restart terminal OS -- DANGEROUS!!!
  LOGOUT          = 5, // Logout from running session
  CANSPAM         = 6, // Spam can commands
  TOPUP           = 7, // TopUp!
  DEBIT           = 8, // Debit
}

enum Lang {
  UA = 0,
  RU = 1,
  EN = 2,
}

enum NotificationChannel {
  ERROR = 0,
  WARNING = 1,
  NEWS_ADMIN = 2,
  NEWS_USER = 3,
}

struct BvParams {
  1: i32 bvCapacity,
  2: i32 bvBanknots,
  3: bool bvConnected,
  4: bool bvStatus,
}

struct TerminalProfile {
  1: string          id,
  2: bool            enabled,
  3: Lang            language,
  4: string          administratorPhone,
}

struct TerminalState {
  1: string          name,
  2: bool            enabled,
  3: Lang            language,
  4: double          totalMoney,
  5: double          currentMoney,
  6: TerminalAction  action,
  7: list<list<i32>> canCmds,
  8: string          payUrl,
  9: BvParams        bvParams,
}

service Terminal {
  TerminalProfile auth(1: string name, 2: string password),
  string getPayUrl(1: string name),
  string getPrograms(1: string terminalId),
  //list<ProgramGroup> getProgramGroups(1: string terminalId),

  void ping(),

  // We should call update on each 500ms or less
  TerminalState update(1: string terminalId),

  oneway void setLang(1: string terminalId, 2: Lang language),
  
  // if action != NONE, we should process it and confirm
  oneway void confirmAction(1: string terminalId, 2: TerminalAction action),

  oneway void setBv(1: string terminalId, 2: BvParams bvParams),
  
  oneway void cashTopUp(1: string terminalId, 2: i32 money, 3: BvParams bvParams),
  oneway void debit(1: string terminalId, 2: double debit),
  oneway void notification(1: string terminalId, 2: NotificationChannel channel, 3: string message),
  oneway void workTime(1: string terminalId, 2: i32 seconds),

  oneway void snd(1: string terminalId, 2: SND snd),
  oneway void svd(1: string terminalId, 2: SVD svd),

  oneway void logout(1: string terminalId),
}