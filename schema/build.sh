#!/usr/bin/env bash

SCRIPT="$(readlink -f "${BASH_SOURCE[0]}")"
DIR="$(dirname "$SCRIPT")"

WASHTERM_SCHEMA_DIR="$DIR/../app/washterm/backend/src/rpc/schema"
SERVER_SCHEMA_DIR="$DIR/../app/server/src/rpc/schema"

mkdir -p "$WASHTERM_SCHEMA_DIR" "$SERVER_SCHEMA_DIR"

thrift -r --gen js:node,ts -out "$WASHTERM_SCHEMA_DIR" "$DIR/terminal.thrift"
thrift -r --gen js:node,ts -out "$SERVER_SCHEMA_DIR" "$DIR/terminal.thrift"