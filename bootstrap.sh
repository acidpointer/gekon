#!/usr/bin/env bash

# Bootstrap!

SCRIPT="$(readlink -f "${BASH_SOURCE[0]}")"
DIR="$(dirname "$SCRIPT")"

DATADIR="$DIR/.data"

function create_dir () {
    local dir="$DATADIR/$1"
    echo "New directory [$2]: -> $dir"
    mkdir -p "$dir"
    chmod "$2" "$dir"
}

function create_file () {
    local file="$DATADIR/$1"
    echo "New file ["$2"]: -> $file"
    touch "$file"
    chmod "$2" "$file"
}

function generate_keys () {
    local key_dir="$DATADIR/$1"
    local priv_key="$key_dir/.priv.pem"
    local pub_key="$key_dir/.pub.pem"

    openssl genrsa -out "$priv_key" 4096 &> /dev/null
    openssl rsa -in "$priv_key" -pubout -out "$pub_key" &> /dev/null
}

function bootstrap () {
    create_dir "log" "775"
    create_dir "db" "775"
    create_dir "db/influxdb/tmp" "777"
    create_dir "db/influxdb/data" "777"
    create_dir "db/mosquitto" "777"
    create_dir "db/grafana" "777"
    create_dir "db/mongodb" "777"
    create_dir "acme" "775"
    create_dir "key" "775"
    create_dir "state" "775"

    create_dir "../app/adminui/build" "775"

    #create_file "db/gf.data" "777"
    #create_file "db/gf.data-journal" "777"
    create_file "acme/acme.json" "600"

    generate_keys "key"
}



function default_env () {
    cat << EOENV > "$DIR/.env"
# Default environment file
# Created at $(date '+%d-%m-%Y %H:%M:%S')
# All passwords generated with openssl

# Domain stuff
ROOT_DOMAIN=https://gekon.one/
DOMAIN_EMAIL=Delytalet@gmail.com
DOMAIN_LIST=gekon.one

# Grafana
GF_ADMIN_USER=admin
GF_ADMIN_PASSWORD=$(openssl rand -hex 13)

# Control panel
CP_ADMIN_PASSWORD=$(openssl rand -base64 14)

# Mongodb
MONGO_ADMIN_USER=root
MONGO_ADMIN_PASSWORD=$(openssl rand -hex 18)

MONGO_USER=gekon_mongo
MONGO_PASS=$(openssl rand -hex 17)

MONGO_DATABASE=gekon

# Mongo-express
ME_USER=admin
ME_PASS=$(openssl rand -hex 12)

# InfluxDB

INFLUXDB_ORG=gekon

## WARNING! InfluxDB will store data for 1 week by default!!!
INFLUXDB_RETENTION=1w

INFLUXDB_ADMIN_USER=admin
INFLUXDB_ADMIN_PASS=$(openssl rand -hex 15)
INFLUXDB_ADMIN_TOKEN=$(openssl rand -hex 21)

INFLUXDB_BUCKET=system

EOENV
}

bootstrap
default_env