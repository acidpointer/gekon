#!/usr/bin/env bash

SCRIPT="$(readlink -f "${BASH_SOURCE[0]}")"
DIR="$(dirname "$SCRIPT")"

adminui () {
    cd "$DIR/app/adminui"

    yarn
    yarn build
}

washterm () {
    cd "$DIR/app/washterm/frontend"

    export REACT_APP_TEST_ENV=true
    export PUBLIC_URL=/wt

    yarn
    yarn build
}

washterm