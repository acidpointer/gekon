import jsonServerProvider from 'ra-data-json-server';

import { Admin, Resource, usePermissions } from 'react-admin';

import { createTheme } from '@material-ui/core/styles';

import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

//@ts-ignore
import russianMessages from 'ra-language-russian';

import polyglotI18nProvider from 'ra-i18n-polyglot';

import AppsIcon from '@material-ui/icons/Apps';
import SettingsIcon from '@material-ui/icons/Settings';
import SettingsCell from '@material-ui/icons/SettingsCell';
import Face from '@material-ui/icons/Face';
import Store from '@material-ui/icons/Store';
import { Receipt } from '@material-ui/icons';
import { LocalAtm } from '@material-ui/icons';

import { Dashboard } from './pages/Dashboard';


import { LoginPage } from './pages/LoginPage';
import { authProvider, httpClient } from './providers/auth-provider';

// Pages
import { ProgramCreate } from './pages/ProgramsCreate';
import { ProgramsList } from './pages/ProgramsList';
import { ProgramsEdit } from './pages/ProgramsEdit';
import { ConfigList } from './pages/ConfigList';
import { ConfigEdit } from './pages/ConfigEdit';
import { TerminalList } from './pages/TerminalList';
import { TerminalCreate } from './pages/TerminalCreate';
import { TerminalEdit } from './pages/TerminalEdit';
import { CompanyList } from './pages/CompanyList';
import { CompanyCreate } from './pages/CompanyCreate';
import { CompanyEdit } from './pages/CompanyEdit';
import { UserList } from './pages/UserList';
import { UserCreate } from './pages/UserCreate';
import { UserEdit } from './pages/UserEdit';
import { OrdersList } from './pages/OrdersList';
import { TransactionList } from './pages/TransactionList';

const dataProvider = jsonServerProvider('/admin', httpClient);

const i18nProvider = polyglotI18nProvider(() => russianMessages, 'ru');

const App = () => {

    const theme = createTheme({
        palette: {
            type: 'dark', // Switching the dark mode on is a single property value change.
            // primary: {
            //     main: '#01b37c',
            // },
            // secondary: {
            //     main: '#ff2200',
            // },
            primary: pink,
            secondary: green,
            error: red,
        },
    });

    const { loading, permissions } = usePermissions();

    return loading ? (<div>Пьем водичьку...</div>) : (
        <Admin theme={theme} loginPage={LoginPage} dashboard={Dashboard} dataProvider={dataProvider} authProvider={authProvider} disableTelemetry i18nProvider={i18nProvider}>

            {permissions => {
                return [
                    permissions[0] === 'ROOT' ?
                        <Resource icon={Store} options={{ label: 'Компании' }} name="company" list={CompanyList} create={CompanyCreate} edit={CompanyEdit} />
                        : null,

                    permissions[0] === 'ROOT' || permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'USERS_LIST') ?
                        <Resource
                            icon={Face}
                            options={{ label: 'Сотрудники' }}
                            name="user"
                            list={UserList}
                            create={permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'USERS_UPDATE') ? UserCreate : undefined}
                            edit={permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'USERS_UPDATE') ? UserEdit : undefined}
                        />
                        : null,

                    permissions[0] === 'ROOT' || permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'TERMINAL_UPDATE') ?
                        <Resource
                            icon={SettingsCell}
                            options={{ label: 'Посты' }}
                            name="terminal" list={TerminalList}
                            edit={permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'TERMINAL_UPDATE') ? TerminalEdit : undefined}
                            create={permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'TERMINAL_UPDATE') ? TerminalCreate : undefined}
                        />
                        : null,

                    permissions[0] === 'ROOT' || permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'PROGRAMS_UPDATE') ?
                        <Resource
                            icon={AppsIcon}
                            options={{ label: 'Программы' }}
                            name="programs"
                            list={ProgramsList}
                            create={ProgramCreate}
                            edit={ProgramsEdit}
                        />
                        : null,
                    permissions[0] === 'OWNER' ?
                        <Resource
                            icon={Receipt}
                            options={{ label: 'Онлайн платежи' }}
                            name="payorder"
                            list={OrdersList}
                        />
                        : null,

                    permissions[0] === 'ROOT' ?
                        <Resource
                            icon={LocalAtm}
                            options={{ label: 'Транзакции' }}
                            name="transaction"
                            list={TransactionList}
                        />
                        : null,

                    permissions[0] === 'ROOT' || permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'CONFIG_UPDATE') ?
                        <Resource
                            icon={SettingsIcon}
                            options={{ label: 'Параметры' }}
                            name="config"
                            list={ConfigList}
                            edit={ConfigEdit}
                        />
                        : null,
                ];
            }}

        </Admin>
    )
};

export default App;
