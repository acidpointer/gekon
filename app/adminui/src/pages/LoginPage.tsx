import { Login } from 'react-admin';

export const LoginPage = () => (
    <Login
        // A random image that changes everyday
        backgroundImage="https://source.unsplash.com/random/1920x1080/daily"
    />
);