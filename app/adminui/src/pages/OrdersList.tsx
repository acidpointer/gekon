import { List, Datagrid, TextField, TextInput, DateField, NumberField } from 'react-admin';

import Typography from '@material-ui/core/Typography';

const Aside = () => (
    <div style={{ width: 200, margin: '1em' }}>
        <Typography variant="h6">Платежи</Typography>
        <Typography variant="body2">
            Журнал успешно подтверженных платежей
        </Typography>
    </div>
);

const filters = [
    <TextInput label="OrderID" source="orderId" alwaysOn />,
    <TextInput label="PayID" source="payId" alwaysOn />,
    <TextInput label="Получатель" source="target" alwaysOn />,
];

export const OrdersList = (props: any) => (
    <List aside={<Aside />} {...props} filters={filters} bulkActionButtons={false}>
        <Datagrid rowClick="edit">
            <TextField label="OrderID" source="orderId" />
            <TextField label="PayID" source="payId" />
            <NumberField label="Сумма" source="amount" />
            <TextField label="Тип получателя" source="type" />
            <TextField label="Получатель" source="target" />
            <DateField label="Дата" source="created_at" showTime />
        </Datagrid>
    </List>
);