import { Edit, required, SaveButton, SimpleForm, TextInput, Toolbar, NumberInput } from 'react-admin';

const ConfigEditToolbar = (props: any) => (
    <Toolbar {...props}>
        <SaveButton disabled={props.pristine}/>
    </Toolbar>
);

const EditTitle = ({ record }: any) => {
    return <span>Редактировать {record ? `"${record.name}"` : ''}</span>;
};

export const ConfigEdit = (props: any) => {
    return (
        <Edit undoable={false} title={<EditTitle />} {...props}>
            <SimpleForm toolbar={<ConfigEditToolbar />}>
                <NumberInput label="Значение" source="value" validate={required()} />
            </SimpleForm>   
        </Edit>
    );
}