import { List, Datagrid, NumberField, TextField, TextInput, DateField } from 'react-admin';

import Typography from '@material-ui/core/Typography';

const Aside = () => (
    <div style={{ width: 200, margin: '1em' }}>
        <Typography variant="h6">Организации</Typography>
        <Typography variant="body2">
            Организации группируют посты и программы для конкретных объектов(Предприятий).
        </Typography>
    </div>
);

const filters = [
    <TextInput label="Пост" source="terminal" alwaysOn />,
];

export const CashList = (props: any) => (
    <List aside={<Aside />} {...props} filters={filters} bulkActionButtons={false}>
        <Datagrid rowClick="edit">
            <TextField label="Терминал" source="terminal" />
            <NumberField label="Внесено" source="amount" />
            <DateField label="Дата" source="created_at" showTime />
        </Datagrid>
    </List>
);
