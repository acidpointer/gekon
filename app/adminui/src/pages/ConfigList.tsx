import { List, Datagrid, TextField, NumberField } from 'react-admin';

export const ConfigList = (props: any) => (
    <List {...props} bulkActionButtons={false} sort={{ field: 'index', order: 'ASC' }}>
        <Datagrid rowClick="edit" >
            <NumberField label="№" source="index" />
            <TextField label="Имя" source="name" />
            <NumberField label="Значение" source="value" />
            <TextField label="Категория" source="category" />
        </Datagrid>
    </List>
);