import { List, Datagrid, TextField, TextInput, NumberField, BooleanField } from 'react-admin';

import Typography from '@material-ui/core/Typography';

// const Aside = () => (
//     <div style={{ width: 200, margin: '1em' }}>
//         <Typography variant="h6">Посты</Typography>
//         <Typography variant="body2">
//             Посты(терминалы) являются клиентскими устройствами
//         </Typography>
//     </div>
// );

const filters = [
    <TextInput label="Имя" source="name" alwaysOn />,
];

export const TerminalList = (props: any) => (
    <List {...props} bulkActionButtons={false}>
        <Datagrid rowClick="edit">
            <TextField label="Имя" source="name" />
            <BooleanField label="Статус" source="enabled" />
            <BooleanField label="Купюроприемник" source="bvConnected" />
            <NumberField label="Купюры" source="bvBanknots" />
            <NumberField label="Макс. купюр" source="bvCapacity" />
            <NumberField label="Общий счетчик моточасов" source="totalWorkHours" />
            <NumberField label="Текущий счетчик моточасов" source="currentWorkHours" />
            <NumberField label="Интервал ТО" source="inspectionInterval" />
        </Datagrid>
    </List>
);