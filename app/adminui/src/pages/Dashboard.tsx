import { Card, CardContent, CardHeader } from '@material-ui/core';
import { usePermissions } from 'ra-core';

export const Dashboard = () => {

    const { loading, permissions } = usePermissions();
    return loading ? (<div>Ждём...</div>) : (
        <Card>
            <CardHeader title="Добро пожаловать в панель управления gekon.one!" />
            <CardContent>
                {permissions[0] === 'ROOT' ? 'Вы вошли под учётной записью с правами суперпользователя! Имейте ввиду, Вы вносите изминения в коммерческую систему, откат изминений не возможен!' :
                    permissions[0] === 'OWNER' ? 'Эта учётная запись предприятия. Используйте её ТОЛЬКО для начальной настройки' :
                        'У вас обычная учётная запись. Руководитель должен был Вас уведомить о возможностях и ограничениях. Удачи!'}
            </CardContent>
        </Card>
    );
}