import { isValidPhoneNumber } from 'libphonenumber-js';
import { AutocompleteArrayInput, ReferenceArrayInput, NumberInput, PasswordInput, BooleanInput, Create, required, SaveButton, SimpleForm, TextInput, Toolbar, useNotify, useRefresh, useRedirect, usePermissions, regex, number, minValue, maxValue } from 'react-admin';

const CompanyCreateToolbar = (props: any) => (
    <Toolbar {...props}>
        <SaveButton disabled={props.pristine} />
    </Toolbar>
);

const CreateTitle = ({ record }: any) => {
    return <span>Регистрация компании</span>;
};

const isValidPhone = (value: string, allValues: string[]): string | undefined => {
    if (isValidPhoneNumber(value)) {
        return undefined;
    }
    return 'Некорректный формат номера';
}


export const CompanyCreate = (props: any) => {
    const notify = useNotify();
    const refresh = useRefresh();
    const redirect = useRedirect();

    const onSuccess = ({ data }: { data: any }) => {
        notify(`Компания "${data.name}" создана!`);
        redirect('/company');
        refresh();
    };

    const validateLogin = [required(), regex(/^[a-zA-Z0-9].{3,14}$/, 'Допустимы только символы латиницы, длина от 4 до 15 символов')];

    const validateMoney = [required(), number(), minValue(0), maxValue(10000)];

    const validatePhone = [required(), isValidPhone];

    return (
        <Create title={<CreateTitle />} onSuccess={onSuccess} mutationMode="pessimistic" {...props} >
            <SimpleForm>
                <NumberInput label="Стоимость тарифа в месяц" source="cost" defaultValue={0} validate={validateMoney} />
                <TextInput label="Имя компании" source="name" validate={required()} />
                <TextInput label="Логин владельца" source="login" validate={validateLogin} />
                <TextInput label="Телефон поддержки" source="administratorPhone" validate={validatePhone} />
                <PasswordInput label="Пароль владельца" source="password" validate={required()}/>
                <BooleanInput label="Активна" source="active"/>
                <ReferenceArrayInput label="Программы" reference="programs" source="programs">
                    <AutocompleteArrayInput />
                </ReferenceArrayInput>
            </ SimpleForm>
        </Create>
    );
}
