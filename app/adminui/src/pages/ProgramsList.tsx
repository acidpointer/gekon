import { List, Datagrid, TextField, TextInput, NumberField } from 'react-admin';

import Typography from '@material-ui/core/Typography';

const Aside = () => (
    <div style={{ width: 200, margin: '1em' }}>
        <Typography variant="h6">Программы</Typography>
        <Typography variant="body2">
            Программы, доступные на постах.
            Различиются по уровням.
        </Typography>
    </div>
);

const postFilters = [
    <TextInput label="Имя" source="name" alwaysOn />,
    <TextInput label="Группа" source="group" defaultValue="waters" />,
];

export const ProgramsList = (props: any) => (
    <List aside={<Aside />} {...props} bulkActionButtons={false}>
        <Datagrid rowClick="edit">
            <TextField label="Имя" source="title.ru" />
            <TextField label="Цена" source="cost" />
        </Datagrid>
    </List>
);