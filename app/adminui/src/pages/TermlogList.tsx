import { List, Datagrid, TextField, TextInput, DateField } from 'react-admin';

import Typography from '@material-ui/core/Typography';

const Aside = () => (
    <div style={{ width: 200, margin: '1em' }}>
        <Typography variant="h6">Журнал событий</Typography>
        <Typography variant="body2">
            Журнал событий постов
        </Typography>
    </div>
);

const filters = [
    <TextInput label="Пост" source="terminal" alwaysOn />,
];

export const TermlogList = (props: any) => (
    <List aside={<Aside />} {...props} filters={filters} bulkActionButtons={false}>
        <Datagrid rowClick="edit">
            <TextField label="ID поста" source="terminal" />
            <TextField label="Уровень" source="level" />
            <TextField label="Секция" source="section" />
            <TextField label="Сообщение" source="message" />
            <DateField label="Время" source="timestamp" showTime />
        </Datagrid>
    </List>
);