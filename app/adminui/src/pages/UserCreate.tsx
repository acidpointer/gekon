import { Create, required, SaveButton, SimpleForm, TextInput, Toolbar, useNotify, useRefresh, useRedirect, SelectInput, AutocompleteArrayInput, minLength, maxLength, PasswordInput, regex,  } from 'react-admin';
import RichTextInput from 'ra-input-rich-text';
import { isValidPhoneNumber } from 'libphonenumber-js';

const TUserCreateToolbar = (props: any) => (
    <Toolbar {...props}>
        <SaveButton disabled={props.pristine} />
    </Toolbar>
);

const CreateTitle = ({ record }: any) => {
    return <span>Регистрация сотрудника</span>;
};

export const UserCreate = (props: any) => {
    const notify = useNotify();
    const refresh = useRefresh();
    const redirect = useRedirect();

    const onSuccess = ({ data }: { data: any }) => {
        notify(`Сотрудник "${data.fullName}" зарегистрирован!`);
        redirect('/user');
        refresh();
    };


    const validateFullName = [required(), minLength(4), maxLength(32)];
    const validateLogin = [required(), regex(/^[a-zA-Z0-9].{3,14}$/, 'Допустимы только символы латиницы, длина от 4 до 15 символов')];
    const validatePass = [required(), minLength(4), maxLength(32)];
    
    const isValidPhone = (value: string, allValues: string[]): string | undefined => {
        if (isValidPhoneNumber(value)) {
            return undefined;
        }
        return 'Некорректный формат номера';
    }

    /*
        - редактор клиентов
        - пополнение счета клиентов
    */

    const validatePhone = [required(), isValidPhone];

    return (
        <Create title={<CreateTitle />} onSuccess={onSuccess} mutationMode="pessimistic" {...props} >
            <SimpleForm>
                <TextInput label="Полное имя" source="fullName" validate={validateFullName} />
                <SelectInput label="Должность" source="role" choices={[
                    { id: 'Собственник', name: 'Собственник' },
                    { id: 'Совладелец', name: 'Совладелец' },
                    { id: 'Ст. Администратор', name: 'Ст. Администратор' },
                    { id: 'Администратор', name: 'Администратор' },
                    { id: 'Тех. персонал', name: 'Тех. персонал' },
                ]} />
                <TextInput label="Телефон" source="phone" inputMode="tel" validate={validatePhone} />
                <TextInput label="Логин" source="name" validate={validateLogin} />
                <PasswordInput label="Пароль" source="password" validate={validatePass} />
                <RichTextInput label="Примечание" source="details" toolbar={[ ['bold', 'italic', 'underline', 'link'] ]}/>
                <AutocompleteArrayInput label="Права" source="permissions" choices={[
                    { id: 'CONFIG_UPDATE', name: 'Конфигурация системы' },
                    { id: 'PROGRAMS_UPDATE', name: 'Конфигуратор программ' },
                    { id: 'TERMINAL_UPDATE', name: 'Настройка постов' },
                    { id: 'USERS_LIST', name: 'Мониторинг сотрудников' },
                    { id: 'USERS_UPDATE', name: 'Редактор сотрудников' },
                    { id: 'CUSTOMERS_LIST', name: 'Мониторинг клиентов' },
                    { id: 'CUSTOMERS_UPDATE', name: 'Пополнение счета клиентов' },
                    { id: 'ALLOW_INSPECTION', name: 'Техническое обслуживание постов' },
                ]} />
            </SimpleForm>
        </Create>
    );
}