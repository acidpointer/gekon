import { isValidPhoneNumber } from 'libphonenumber-js';
import { Edit, SimpleForm, PasswordInput, NumberInput, ReferenceArrayInput, AutocompleteArrayInput, minValue, maxValue, number, required, TextInput } from 'react-admin';

// const CompanyEditToolbar = (props: any) => (
//     <Toolbar {...props}>
//         <SaveButton disabled={props.pristine}/>
//     </Toolbar>
// );

const EditTitle = ({ record }: any) => {
    return <span>Компания {record ? `"${record.name}"` : ''}</span>;
};

const isValidPhone = (value: string, allValues: string[]): string | undefined => {
    if (isValidPhoneNumber(value)) {
        return undefined;
    }
    return 'Некорректный формат номера';
}

export const CompanyEdit = (props: any) => {

    const moneyValidator = [number(), minValue(0), maxValue(10000)];
    const validatePhone = [required(), isValidPhone];

    return (
        <Edit undoable={false} title={<EditTitle />} {...props}>
            <SimpleForm>
                <PasswordInput label="Сменить пароль" source="password" />
                <NumberInput label="Пополнить счёт" source="money" validate={moneyValidator} />
                <TextInput label="Телефон поддержки" source="administratorPhone" validate={validatePhone} />
                <ReferenceArrayInput label="Программы" reference="programs" source="programs">
                    <AutocompleteArrayInput />
                </ReferenceArrayInput>
            </SimpleForm>   
        </Edit>
    );
}