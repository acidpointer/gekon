import { List, Datagrid, TextField } from 'react-admin';

import Typography from '@material-ui/core/Typography';

const Aside = () => (
    <div style={{ width: 200, margin: '1em' }}>
        <Typography variant="h6">Сессии</Typography>
        <Typography variant="body2">
            Показывает задействованность постов клиентами.
        </Typography>
    </div>
);

export const SessionList = (props: any) => (
    <List aside={<Aside />} {...props} bulkActionButtons={false}>
        <Datagrid>
            <TextField label="Пост" source="terminal" />
            <TextField label="Клиент" source="customer" />
        </Datagrid>
    </List>
);