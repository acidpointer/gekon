import { List, Datagrid, TextField, TextInput } from 'react-admin';

import Typography from '@material-ui/core/Typography';

const Aside = () => (
    <div style={{ width: 200, margin: '1em' }}>
        <Typography variant="h6">Персонал</Typography>
        <Typography variant="body2">
            Персонал компании
        </Typography>
    </div>
);

const postFilters = [
    <TextInput label="Имя" source="name" alwaysOn />,
    <TextInput label="Группа" source="group" defaultValue="waters" />,
];

export const UserList = (props: any) => (
    <List aside={<Aside />} {...props} bulkActionButtons={false}>
        <Datagrid rowClick="edit">
            <TextField label="Полное имя" source="fullName" />
            <TextField label="Должность" source="role" />
            <TextField label="Телефон" source="phone" />
            <TextField label="Логин" source="name" />
        </Datagrid>
    </List>
);