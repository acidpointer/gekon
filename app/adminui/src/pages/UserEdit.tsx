import { isValidPhoneNumber } from 'libphonenumber-js';
import RichTextInput from 'ra-input-rich-text';
import { required, SimpleForm, useNotify, useRefresh, useRedirect, Edit, TextInput, SelectInput, AutocompleteArrayInput, PasswordInput } from 'react-admin';

const EditTitle = ({ record }: any) => {
    return <span>Сотрудник {record.phone ? record.phone : ''}</span>;
};

export const UserEdit = (props: any) => {

    const notify = useNotify();
    const refresh = useRefresh();
    const redirect = useRedirect();

    const onSuccess = ({ data }: { data: any }) => {
        notify(`Данные сотрудника обновлены!`);
        redirect('/user');
        refresh();
    };

    const isValidPhone = (value: string, allValues: string[]): string | undefined => {
        if (isValidPhoneNumber(value)) {
            return undefined;
        }
        return 'Некорректный формат номера';
    }

    const validatePhone = [required(), isValidPhone];

    return (
        <Edit undoable={false} title={<EditTitle />} {...props} >
            <SimpleForm>
                <TextInput label="Полное имя" source="fullName" validate={required()} />
                <SelectInput label="Должность" source="role" choices={[
                    { id: 'Собственник', name: 'Собственник' },
                    { id: 'Совладелец', name: 'Совладелец' },
                    { id: 'Ст. Администратор', name: 'Ст. Администратор' },
                    { id: 'Администратор', name: 'Администратор' },
                    { id: 'Тех. персонал', name: 'Тех. персонал' },
                ]} />
                <TextInput label="Телефон" source="phone" inputMode="tel" validate={validatePhone} />
                <PasswordInput label="Пароль" source="password" />
                <RichTextInput label="Примечание" source="details" toolbar={[['bold', 'italic', 'underline', 'link']]} />
                <AutocompleteArrayInput label="Права" source="permissions" choices={[
                    { id: 'CONFIG_UPDATE', name: 'Конфигурация системы' },
                    { id: 'PROGRAMS_UPDATE', name: 'Конфигуратор программ' },
                    { id: 'TERMINAL_UPDATE', name: 'Настройка постов' },
                    { id: 'USERS_LIST', name: 'Мониторинг сотрудников' },
                    { id: 'USERS_UPDATE', name: 'Редактор сотрудников' },
                    { id: 'CUSTOMERS_LIST', name: 'Мониторинг клиентов' },
                    { id: 'CUSTOMERS_UPDATE', name: 'Пополнение счета клиентов' },
                    { id: 'ALLOW_INSPECTION', name: 'Техническое обслуживание постов' },
                ]} />
            </SimpleForm>
        </Edit>
    );
}