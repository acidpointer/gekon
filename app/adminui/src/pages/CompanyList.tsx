import { List, Datagrid, NumberField, TextField, TextInput } from 'react-admin';

import Typography from '@material-ui/core/Typography';

const Aside = () => (
    <div style={{ width: 200, margin: '1em' }}>
        <Typography variant="h6">Организации</Typography>
        <Typography variant="body2">
            Организации группируют посты и программы для конкретных объектов(Предприятий).
        </Typography>
    </div>
);

const filters = [
    <TextInput label="Имя" source="name" alwaysOn />,
];

export const CompanyList = (props: any) => (
    <List aside={<Aside />} {...props} bulkActionButtons={false}>
        <Datagrid rowClick="edit">
            <TextField label="Имя" source="name" />
            <TextField label="Владелец" source="login" />
            <NumberField label="Стоимость тарифа" source="cost" />
            <NumberField label="Баланс" source="money" />
        </Datagrid>
    </List>
);
