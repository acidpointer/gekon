import { AutocompleteArrayInput, Edit, FormTab, maxValue, minValue, number, NumberInput, ReferenceArrayInput, required, SaveButton, SelectArrayInput, SelectInput, SimpleForm, TabbedForm, TextInput, Toolbar } from 'react-admin';

import icons from '../icons.json';

const ProgramEditToolbar = (props: any) => (
    <Toolbar {...props}>
        <SaveButton disabled={props.pristine} />
    </Toolbar>
);

const EditTitle = ({ record }: any) => {
    return <span>Редактировать {record ? `"${record.name}"` : ''}</span>;
};

export const ProgramsEdit = (props: any) => {
    return (
        <Edit undoable={false} title={<EditTitle />} {...props} >
            <TabbedForm>
                <FormTab label="Основное">
                    <TextInput label="Системное имя" source="name" validate={required()} />
                    <ReferenceArrayInput label="Подпрограммы" reference="programs" source="submodes">
                        <AutocompleteArrayInput />
                    </ReferenceArrayInput>
                    <SelectInput label="Цвет кнопки" source="button" choices={[
                        { id: 'default', name: 'По-умолчанию' },
                        { id: 'white', name: 'Белый' },
                        { id: 'pink', name: 'Розовый' },
                        { id: 'yellow', name: 'Желтый' },
                    ]} />
                    <NumberInput label="Цена за минуту" source="cost" validate={[number(), minValue(0), maxValue(500)]} />
                    <TextInput label="Команда запуска" source="startCommand" />
                    <TextInput label="Команда остановки" source="stopCommand" />
                    <SelectInput label="Тип пистолета" source="type" choices={[
                        { id: 'red', name: 'Красный' },
                        { id: 'blue', name: 'Синий' },
                    ]} />
                </FormTab>

                <FormTab label="Локализация">
                    <TextInput label="Имя (рус)" source="title.ru" validate={required()} />
                    <TextInput label="Имя (укр)" source="title.ua" validate={required()} />
                    <TextInput label="Имя (англ)" source="title.en" validate={required()} />
                    <TextInput label="Описание (рус)" source="description.ru" />
                    <TextInput label="Описание (укр)" source="description.ua" />
                    <TextInput label="Описание (англ)" source="description.en" />
                    <TextInput label="Рекомендация (рус)" source="rec.ru" />
                    <TextInput label="Рекомендация (укр)" source="rec.ua" />
                    <TextInput label="Рекомендация (англ)" source="rec.en" />
                </FormTab>

                <FormTab label="Иконки">
                    <SelectInput label="Иконка режима" source="icons.modes" choices={[
                        { id: icons.bubbles, name: 'Пузырьки' },
                        { id: icons.osmosis, name: 'Осмос' },
                        { id: icons.temperature, name: 'Температура' },
                        { id: icons.turbo, name: 'Турбо' },
                        { id: icons.turbo2, name: 'Турбо2' },
                        { id: icons.water, name: 'Вода' },
                        { id: icons.wax, name: 'Воск' },
                        { id: icons.whiteFoam, name: 'Белая пена' },
                    ]} />
                    <SelectInput label="Иконка запуска" source="icons.start" choices={[
                        { id: icons.bubbles, name: 'Пузырьки' },
                        { id: icons.osmosis, name: 'Осмос' },
                        { id: icons.temperature, name: 'Температура' },
                        { id: icons.turbo, name: 'Турбо' },
                        { id: icons.turbo2, name: 'Турбо2' },
                        { id: icons.water, name: 'Вода' },
                        { id: icons.wax, name: 'Воск' },
                        { id: icons.whiteFoam, name: 'Белая пена' },
                    ]} />
                </FormTab>

            </TabbedForm>
        </Edit>
    );
}