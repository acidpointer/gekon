import { Create, required, SaveButton, SimpleForm, TextInput, Toolbar, PasswordInput, NumberInput, SelectInput, BooleanInput, useNotify, useRefresh, useRedirect, regex, number, minValue, maxValue, ReferenceArrayInput, AutocompleteArrayInput } from 'react-admin';

const TerminalCreateToolbar = (props: any) => (
    <Toolbar {...props}>
        <SaveButton disabled={props.pristine} />
    </Toolbar>
);

const CreateTitle = ({ record }: any) => {
    return <span>Регистрация поста</span>;
};

export const TerminalCreate = (props: any) => {
    const notify = useNotify();
    const refresh = useRefresh();
    const redirect = useRedirect();

    const onSuccess = () => {
        notify('Пост зарегистрирован!');
        redirect('/terminal');
        refresh();
    };

    const validateName = [required(), regex(/^[a-zA-Z0-9].{3,14}$/, 'Допустимы только символы латиницы, длина от 4 до 15 символов')];

    return (
        <Create title={<CreateTitle />} onSuccess={onSuccess} {...props} >
            <SimpleForm>
                <TextInput label="Имя поста" source="name" validate={validateName} />
                <PasswordInput label="Пароль" source="password" validate={required()} />
                <NumberInput label="Интервал ТО" source="inspectionInterval" validate={[number(), required(), minValue(0), maxValue(10_000)]} />
                <NumberInput label="Ёмкость купюроприемника" source="bvCapacity" validate={[number(), required(), minValue(0), maxValue(100_000)]} />
                <BooleanInput label="Включен" source="enabled" />
                <ReferenceArrayInput label="Программы" reference="programs" source="programs">
                    <AutocompleteArrayInput />
                </ReferenceArrayInput>
            </SimpleForm>
        </Create>
    );
}