import { Edit, required, SimpleForm, PasswordInput, NumberInput, SelectInput, BooleanInput, number, minValue, maxValue, ReferenceArrayInput, AutocompleteArrayInput } from 'react-admin';

const CreateTitle = ({ record }: any) => {
    return <span>Редактор поста {record.name ? record.name : ''}</span>;
};

export const TerminalEdit = (props: any) => {
    const permissions = props.permissions;
    return (
        <Edit undoable={false} title={<CreateTitle />} {...props} >
            <SimpleForm>
                <PasswordInput label="Пароль" source="password" />
                <NumberInput label="Ёмкость купюроприемника" source="bvCapacity" validate={[number(), required(), minValue(0), maxValue(100_000)]} />
                {permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'ALLOW_INSPECTION') ? <NumberInput label="Интервал ТО" source="inspectionInterval" validate={[number(), required(), minValue(0), maxValue(10_000)]} /> : null}
                <ReferenceArrayInput label="Программы" reference="programs" source="programs">
                    <AutocompleteArrayInput />
                </ReferenceArrayInput>
                <BooleanInput label="Включен" source="enabled" />

                {permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'ALLOW_INSPECTION') ? <BooleanInput label="Снять блокировку DND1" source="fixDnd1Errors" /> : null}
                {permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'ALLOW_INSPECTION') ? <BooleanInput label="Снять блокировку DND2" source="fixDnd2Errors" /> : null}
                {permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'ALLOW_INSPECTION') ? <BooleanInput label="Снять блокировку DND3" source="fixDnd3Errors" /> : null}
                {permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'ALLOW_INSPECTION') ? <BooleanInput label="Снять блокировку DND4" source="fixDnd4Errors" /> : null}
                {permissions[0] === 'OWNER' || !!permissions.find((o: string) => o === 'ALLOW_INSPECTION') ? <BooleanInput label="Проведено тех. обслуживание" source="inspectionComplete" /> : null}
            </SimpleForm>
        </Edit>
    );
}