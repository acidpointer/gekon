import { List, Datagrid, TextField, TextInput, DateField, NumberField } from 'react-admin';

const filters = [
    <TextInput label="PayID" source="payId" alwaysOn />,
    <TextInput label="ID транзакции" source="transaction_id" alwaysOn />,
    <TextInput label="Карта" source="sender_card_mask2" alwaysOn />,
    <TextInput label="Банк" source="sender_card_bank" alwaysOn />,
    <TextInput label="Описание" source="description" alwaysOn />,
];

export const TransactionList = (props: any) => (
    <List {...props} filters={filters} bulkActionButtons={false}>
        <Datagrid rowClick="edit">
            <TextField label="PayID" source="payId" />
            <TextField label="ID транзакции" source="transaction_id" />
            <TextField label="Действие" source="action" />
            <NumberField label="Сумма" source="amount" />
            <TextField label="Валюта" source="currency" />
            <TextField label="Статус" source="status" />
            <TextField label="Описание" source="description" />
            <TextField label="Карта" source="sender_card_mask2" />
            <TextField label="Банк" source="sender_card_bank" />
            <DateField label="Дата" source="created_at" showTime />
        </Datagrid>
    </List>
);