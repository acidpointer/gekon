import { fetchUtils } from "ra-core";

export const authProvider = {
    // called when the user attempts to log in
    login: async ({ username, password }: { username: string, password: string }) => {

        const request = new Request('/admin/auth', {
            method: 'POST',
            body: JSON.stringify({ username, password }),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        });

        try {
            const response = await fetch(request);
            if (response.status < 200 || response.status >= 300) {
                throw new Error(response.statusText);
            }
            const auth = await response.json();

            localStorage.setItem('auth', JSON.stringify(auth));
            localStorage.setItem('permissions', JSON.stringify(auth.permissions));
        } catch (e) {
            throw new Error('Authorization failed!');
        }
    },
    // called when the user clicks on the logout button
    logout: () => {
        localStorage.removeItem('auth');
        localStorage.removeItem('permissions');
        return Promise.resolve();
    },
    // called when the API returns an error
    checkError: ({ status }: { status: number }) => {
        if (status === 401 || status === 403) {
            localStorage.removeItem('auth');
            localStorage.removeItem('permissions');
            return Promise.reject();
        }
        return Promise.resolve();
    },
    // called when the user navigates to a new location, to check for authentication
    checkAuth: () => {
        return localStorage.getItem('auth')
            ? Promise.resolve()
            : Promise.reject();
    },
    getPermissions: () => {
        const role = localStorage.getItem('permissions');
        return role && role.length ? Promise.resolve(JSON.parse(role)) : Promise.reject();
    },
    getIdentity: () => {
        try {
            const { id, fullName, username } = JSON.parse(localStorage.getItem('auth') as string);
            return Promise.resolve({ id, fullName, username });
        } catch (error) {
            return Promise.reject(error);
        }
    }
};

export const httpClient = (url: string, options: any = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    const { token } = JSON.parse(localStorage.getItem('auth') as string);
    options.headers.set('Authorization', `Bearer ${token}`);
    return fetchUtils.fetchJson(url, options);
};