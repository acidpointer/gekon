import { FastifyReply, FastifyRequest } from 'fastify';

import Single from '../../../models/single.model';
import Company from '../../../models/company.model';

import { IConfig, IParameter } from '../../../models/types';
import { dynamicSort } from '../../../utils/arrays';

import { IGetListQuery } from './types';
import { cacheConfig, checkPermission, getConfig, getUserContext } from '../../../utils';
import { GekonPermission } from '../../types';

export const getList = async (req: FastifyRequest<{ Querystring: IGetListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.CONFIG_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { companyId } = getUserContext(req);

        const { _end, _order, _sort, _start } = req.query;
        const params = await getConfig(usr.isRoot ? undefined : companyId);
        if (params) {
            return reply
                .code(200)
                .header('X-Total-Count', params.length)
                .send(params
                    .slice(Number(_start), Number(_end))
                    .sort(dynamicSort(_sort, _order)));
        }
        return reply.code(500).send('Parameters not found');
    } catch (err) {
        req.log.error(`config.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.CONFIG_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { companyId } = getUserContext(req);
        const params = await getConfig(usr.isRoot ? undefined : companyId);

        if (params) {
            const { id } = req.params;
            const param = params.find((o: IParameter) => o.id === id);
            if (param) {
                return reply.code(200).send(param);
            }
            return reply.code(500).send(`Parameter ${id} not found!`);
        }
        return reply.code(500).send('Parameter not found');
    } catch (err) {
        req.log.error(`config.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const update = async (req: FastifyRequest<{ Params: { id: string }, Body: { value: number } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.CONFIG_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { companyId } = getUserContext(req);

        const company = usr.isRoot ? undefined : companyId;
        const currentParams = await getConfig(company);

        const { id } = req.params;
        const { value } = req.body;

        if (currentParams) {
            const changedParameter = currentParams.find((o: IParameter) => o.id === id);
            if (changedParameter) {
                if (Number(changedParameter.value) === NaN || Number(value) === NaN) {
                    return reply.code(500).send('Wrong value type');
                }
                const newParams: IParameter[] = currentParams.map((p: IParameter) => (
                    p.id === id ? {
                        ...p,
                        value,
                    } : p
                ));

                if (company) {
                    const comp = await Company.findById(company);
                    if (!comp) {
                        return reply.code(500).send('Company not found!');
                    }

                    await Company.updateOne({ _id: company }, {
                        config: newParams,
                    });
                } else {
                    const item: IConfig = { name: 'config', parameters: newParams };
                    await Single.updateOne({ name: 'config' }, { item });
                    await cacheConfig();
                }
                return reply.code(200).send(newParams);
            }
        }
        return reply.code(500).send('Parameters not found!');
    } catch (err) {
        req.log.error(`config.update err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}