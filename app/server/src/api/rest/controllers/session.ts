import { FastifyReply, FastifyRequest } from 'fastify';
import { IGetListQuery } from './types';

import { dynamicSort } from '../../../utils/arrays';
import { checkPermission, getUserContext } from '../../../utils';
import { GekonPermission } from '../../types';

export const getList = async (req: FastifyRequest<{ Querystring: IGetListQuery }>, reply: FastifyReply) => {
    // try {
    //     const usr = getUserContext(req);
    //     if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.SESSION_LIST))) {
    //         return reply.code(500).send('Permission denied!');
    //     }

    //     const { _end, _order, _sort, _start } = req.query;
    //     const sessions = getSessionManager().getState();
    //     return reply
    //         .code(200)
    //         .header('X-Total-Count', sessions.length)
    //         .send(
    //             sessions
    //                 .slice(Number(_start), Number(_end))
    //                 .sort(dynamicSort(_sort, _order)));
    // } catch (err) {
    //     req.log.error(`session.getList err: ${err}`);
    //     return reply.code(500).send(`${err}`);
    // }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    // try {
    //     const usr = getUserContext(req);
    //     if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.SESSION_LIST))) {
    //         return reply.code(500).send('Permission denied!');
    //     }

    //     const { id } = req.params;
    //     const sessions = getSessionManager().getState();
    //     return reply.code(200).send(sessions.find(o => o.id === id));
    // } catch (err) {
    //     req.log.error(`session.getOne err: ${err}`);
    //     return reply.code(500).send(`${err}`);
    // }
}