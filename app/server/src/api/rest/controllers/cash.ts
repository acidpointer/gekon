import { FastifyReply, FastifyRequest } from 'fastify';

import Cash from '../../../models/cash.model';
import { getUserContext } from '../../../utils';

import { IGetListQuery } from './types';

export interface ICashListQuery extends IGetListQuery {
    terminal: string;
}

export const getList = async (req: FastifyRequest<{ Querystring: ICashListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner)) {
            return reply.code(500).send('Permission denied!');
        }

        const { _start, _end, _sort, _order, terminal } = req.query;

        let search = { companyId: usr.companyId } as { companyId?: string, terminal?: object };

        if (terminal && terminal.length) {
            search = { ...search, terminal: { $regex: terminal, $options: 'i' } };
        }

        const cash = await Cash.find(search, null, { skip: Number(_start), limit: Number(_end), sort: { [_sort]: _order.toLowerCase() } })

        if (cash) {
            const count = await Cash.countDocuments(search);
            return reply.code(200).header('X-Total-Count', count).send(cash?.map(o => ({
                ...o._doc,
                id: o.id,
                _id: undefined,
                _v: undefined,
            })));
        }
    } catch (err) {
        req.log.error(`cash.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner)) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;

        const cash = await Cash.findById(id);
        if (cash) {
            return reply.code(200).send({
                ...cash._doc,
                id: cash.id,
                _id: undefined,
                _v: undefined,
            });
        }
    } catch (err) {
        req.log.error(`cash.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}
