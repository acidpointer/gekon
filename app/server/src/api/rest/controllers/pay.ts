import { FastifyReply, FastifyRequest } from 'fastify';
import redis from '../../../redis';

import Terminal from '../../../models/terminal.model';

import { getConfigValue } from '../../../utils';
import { GekonServer } from '../../../app';


export const liqPayConfirm = async (req: FastifyRequest<{ Body: { data: string, signature: string } }>, reply: FastifyReply) => {
    try {
        const { data, signature } = req.body;

        const confirm = await GekonServer.instance.pay.confirm(data, signature);

        if (confirm) {
            const { type, target, amount, orderId } = confirm;

            if (amount > 999) {
                throw new Error(`Payment can't be greather than 999. Current: ${amount}`);
            }

            switch (type) {
                case 'terminal': {
                    const terminal = await Terminal.findOne({ name: target });
                    if (terminal) {
                        const newMoney = terminal.money + amount;
                        await Terminal.updateOne({ name: terminal.name }, { money: newMoney });
                        GekonServer.instance.handler.topUp(terminal.id, amount);                       
                        GekonServer.instance.mailings.notification(`Счёт поста ${terminal.name} пополнен на ${amount} и составляет ${newMoney}\n` +
                            `ID заказа ${orderId}`);
                    } else {
                        req.log.error(`pay.confirm terminal ${target} not found!`);
                    }
                    break;
                }
            }
        } else {
            req.log.error('pay.confirm confirm is null!');
        }
    } catch (err) {
        req.log.error(`pay.confirm err: ${err}`);
    }
    return reply.code(200).send('ok');
}

export const payPage = async (req: FastifyRequest<{ Params: { token: string } }>, reply: FastifyReply) => {
    try {
        const { token } = req.params;

        const td = await redis.get(`pay-${token}`);
        if (td) {
            const avail = await GekonServer.instance.pay.getTokenTarget(token);
            if (avail) {
                return reply.view('payment/index', {
                    token,
                    target: avail.target,
                });
            }
        }
    } catch (err) {
        req.log.error(`pay.payPage err: ${err}`);
    }
    return reply.code(404).send('Not found!');
}

export const payData = async (req: FastifyRequest<{ Body: string }>, reply: FastifyReply) => {
    try {
        const { token, amount } = JSON.parse(req.body) as { token: string, amount: number };

        const avail = token && token.length && amount > 0 ? await GekonServer.instance.pay.getTokenTarget(token) : false;
        if (avail) {
            const { type, target, companyId } = avail;
            const serviceData = await GekonServer.instance.pay.getData(amount, type, target, companyId, token);
            if (serviceData) {
                const { data, signature } = serviceData;
                return reply.code(200).send({ data, signature });
            }
        }
    } catch (err) {
        req.log.error(`pay.payData err: ${err}`);
    }
    return reply.code(304).send('nope!');
}

export const testUrl = async (req: FastifyRequest, reply: FastifyReply) => {
    try {
        const token = await GekonServer.instance.pay.createToken({ type: 'terminal', target: 'test-1', companyId: 'some' }, 'test-1');
        if (token) {
            return reply.redirect(`https://gekon.one/pay/${token}`);
        }
    } catch (err) {
        req.log.error(`pay.testUrl err: ${err}`);
        return reply.code(500).send('Internal error!');
    }
}