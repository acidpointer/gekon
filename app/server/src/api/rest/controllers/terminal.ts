import { FastifyReply, FastifyRequest } from 'fastify';

import Terminal from '../../../models/terminal.model';

import { dynamicSort } from '../../../utils/arrays';
import { GekonPermission } from '../../types';
import { checkPermission, getCompanyPrograms, getUserContext } from '../../../utils';

import { IGetListQuery } from './types';
import { scryptHash } from '../../../utils/crypto';
import { GekonServer } from '../../../app';

export const getList = async (req: FastifyRequest<{ Querystring: IGetListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.TERMINAL_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { _start, _end, _sort, _order } = req.query;

        const handler = GekonServer.instance.handler;

        const search = usr.isRoot ? {} : { companyId: usr.companyId };

        const terminals = await Terminal.find(search);
        const count = await Terminal.countDocuments(search);
        if (terminals) {
            return reply
                .code(200)
                .header('X-Total-Count', count)
                .send(terminals
                    .slice(Number(_start), Number(_end))
                    .sort(dynamicSort(_sort, _order))
                    .map(o => ({
                            ...o._doc,
                            bvConnected: !!handler.state.get(o.id)?.bvParams.bvConnected,
                            bvBanknots: o.bvBanknots,
                            bvCapacity: o.bvCapacity,
                            enabled: o.enabled && !!handler.state.get(o.id)?.enabled,
                            id: o.id,
                            _id: undefined,
                            _v: undefined,
                            password: undefined,
                        })));
        }
        return reply.code(500).send('Terminals not found error!');
    } catch (err) {
        req.log.error(`terminal.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.TERMINAL_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;

        const search = usr.isRoot ? { _id: id } : { _id: id, companyId: usr.companyId };

        const terminal = await Terminal.findOne(search);
        if (terminal) {
            return reply.code(200).send({
                ...terminal._doc,
                inspectionComplete: false,
                password: undefined,
                id: terminal.id,
                _id: undefined,
                _v: undefined,
            });
        }
        return reply.code(500).send(`Terminal [${id}] not found`);
    } catch (err) {
        req.log.error(`terminal.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export interface ITerminalCreate {
    Body: {
        name: string;
        password: string;
        enabled: boolean;
        programs: string[];
        inspectionInterval: number;
        bvCapacity: number;
    }
}

export const create = async (req: FastifyRequest<ITerminalCreate>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || checkPermission(usr, GekonPermission.TERMINAL_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { name, password, enabled, inspectionInterval, programs, bvCapacity } = req.body;

        //const availPrograms = await getCompanyPrograms(usr.companyId);

        const avail = await Terminal.find({ name });
        if (avail && avail.length) {
            return reply.code(500).send('Terminal already exist!');
        }

        const terminal = await Terminal.create({
            name,
            password: await scryptHash(password),
            companyId: usr.companyId,
            enabled,
            inspectionInterval,
            money: 0,
            programs: programs ?? [],
            bvCapacity,
        });

        if (terminal) {
            return reply.code(200).send({
                ...terminal._doc,
                password: undefined,
                busy: false,
                id: terminal.id,
                _id: undefined,
                _v: undefined
            });
        }
        return reply.code(500).send('Cant register new terminal!');
    } catch (err) {
        req.log.error(`terminal.create err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export interface ITerminalUpdate {
    Params: {
        id: string;
    },
    Body: {
        password: string;
        enabled: boolean;
        inspectionComplete: boolean;
        programs: string[];

        fixDnd1Errors: boolean;
        fixDnd2Errors: boolean;
        fixDnd3Errors: boolean;
        fixDnd4Errors: boolean;

        inspectionInterval: number;

        bvCapacity: number;
    }
}

export const update = async (req: FastifyRequest<ITerminalUpdate>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || checkPermission(usr, GekonPermission.TERMINAL_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;
        const { password, enabled, inspectionInterval, inspectionComplete, bvCapacity, programs } = req.body;
        const { fixDnd1Errors, fixDnd2Errors, fixDnd3Errors, fixDnd4Errors } = req.body;

        const updatePass = !!password?.length;

        const terminal = await Terminal.findOne({ _id: id, companyId: usr.companyId });
        if (terminal) {
            const upd = {
                password: updatePass ? await scryptHash(password) : terminal.password,
                programs: programs ?? [],
                enabled,
                inspectionInterval,
                bvCapacity,
            };

            await Terminal.updateOne({ _id: id, companyId: usr.companyId }, inspectionComplete ? {
                ...upd,
                currentWorkHours: 0,
                lastInspection: new Date(),
            } : upd);
            if (programs && programs?.length > 0) {
                GekonServer.instance.handler.restart(terminal.id);
            }

            if (enabled !== terminal.enabled) {
                GekonServer.instance.mailings.warning(`Пост ${terminal.name} ${enabled ? 'включен' : 'выключен'}!`);
                if (enabled) {
                    GekonServer.instance.handler.start(terminal.id);
                } else {
                    GekonServer.instance.handler.stop(terminal.id);
                }
            }

            if (fixDnd1Errors || fixDnd2Errors || fixDnd3Errors || fixDnd4Errors) {
                let c = [];
                let descrs: string[] = [];
                if (fixDnd1Errors) {
                    c.push([0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x00]);
                    descrs.push('DND1');
                }
                if (fixDnd2Errors) {
                    c.push([0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x00]);
                    descrs.push('DND2');
                }
                if (fixDnd3Errors) {
                    c.push([0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 0x00]);
                    descrs.push('DND3');
                }
                if (fixDnd4Errors) {
                    c.push([0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x00]);
                    descrs.push('DND4');
                }
                GekonServer.instance.handler.canSpam(terminal.id, c);
            }

            return reply.code(200).send({
                ...terminal._doc,
                password: undefined,
                id: terminal.id,
                _id: undefined,
                _v: undefined
            });
        }
        return reply.code(500).send('Cant update terminal!');
    } catch (err) {
        req.log.error(`terminal.update err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const deleteOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || checkPermission(usr, GekonPermission.TERMINAL_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;

        const del = await Terminal.findOneAndDelete({ _id: id, companyId: usr.companyId });
        if (del) {
            return reply.code(200).send({
                ...del._doc,
                password: undefined,
                id: del.id,
                _id: undefined,
                _v: undefined,
            });
        }
        return reply.code(500).send('Cant delete terminal!');
    } catch (err) {
        req.log.error(`terminal.deleteOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}