import { FastifyRequest, FastifyReply } from 'fastify';

import { IGetListQuery } from './types';

import Company, { ICompanyDocument } from '../../../models/company.model';
import Single from '../../../models/single.model';
import Program from '../../../models/program.model';
import User from '../../../models/user.model';
import Terminal from '../../../models/terminal.model';
import Termlog from '../../../models/termlog.model';
import PayOrder from '../../../models/payorder.model';

import { getCompanyPrograms, getConfig, getUserContext } from '../../../utils';
import { scryptHash } from '../../../utils/crypto';
import { IProgramGroups, IPrograms } from '../../../models/types';

export const getList = async (req: FastifyRequest<{ Querystring: IGetListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isRoot || usr.isOwner)) {
            return reply.code(500).send('Permision denied!');
        }

        const { _sort, _end, _order, _start } = req.query;

        const { companyId } = getUserContext(req);
        const search = usr.isRoot ? {} : { _id: companyId };

        const companies = await Company.find(search, null, { skip: Number(_start), limit: Number(_end), sort: { [_sort]: _order.toLowerCase() } });

        if (companies) {
            const count = await Company.countDocuments(search);

            return reply.code(200).header('X-Total-Count', count).send(companies?.map((o: any) => {
                const { name, money, cost, login } = o;
                const programs = o.programs.map((p: any) => ({ ...p, id: p._id }));
                return {
                    id: o?._id.toString(),
                    name,
                    login,
                    money: money > 0 ? money : 0,
                    cost: cost > 0 ? cost : 0,
                    programs: programs ? programs : [],
                }
            }));
        }
        return reply.code(500).send('Companies not found!');
    } catch (err) {
        req.log.error(`company.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isRoot || usr.isOwner)) {
            return reply.code(500).send('Permision denied!');
        }

        const { id } = req.params;
        const company = await Company.findById(id);
        if (company) {
            return reply.code(200).send({
                ...company._doc,
                programs: company.programs.map((o: any) => (o._id)),
                id: company.id,
                password: undefined,
                _id: undefined,
                _v: undefined,
            });
        }
        return reply.code(500).send('Company not found!');
    } catch (err) {
        req.log.error(`company.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export interface ICompanyCreate {
    Body: {
        name: string;
        login: string;
        password: string;
        cost: number;
        money: number;
        active: boolean;
        administratorPhone: string;
        programs: string[];
    }
}

export const create = async (req: FastifyRequest<ICompanyCreate>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!usr.isRoot) {
            return reply.code(500).send('Permision denied!');
        }

        // Fetch config and programs from redis(or mongo)
        const config = await getConfig();
        if (!config) {
            return reply.code(500).send('Config not found!');
        }

        const progs = await getCompanyPrograms();


        // We're ready to create company!
        const { name, login, password, cost, money, active, programs, administratorPhone } = req.body;

        const availUsers = await User.find({ name: login });
        if (login === 'admin' || (availUsers && availUsers.length)) {
            return reply.code(500).send(`Wrong login '${login}' because it is already exist!`);
        }

        const availCompany = await Company.findOne({ login });
        if (availCompany) {
            return reply.code(500).send(`Company '${name}' already exist!`);
        }

        const company = await Company.create({
            name,
            login,
            password: await scryptHash(password),
            cost: cost > 1 ? cost : 1,
            money: money > 0 ? money : 0,
            active,
            programs: progs.filter(pr => programs.find(o => o === pr.id.toString())),
            config,
            administratorPhone,
        });
        if (company) {
            return reply.code(200).send({
                ...company._doc,
                id: company.id,
                _id: undefined,
                _v: undefined,
            });
        }
        req.log.error(`Can't create company ${name}`);
        return reply.code(500).send(`Can't create company ${name}`);
    } catch (err) {
        req.log.error(`company.create err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export interface ICompanyUpdate {
    Params: {
        id: string,
    },
    Body: {
        password: string,
        cost: number,
        money: number,
        administratorPhone: string,
        programs: string[],
    }
}

export const update = async (req: FastifyRequest<ICompanyUpdate>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isRoot || usr.isOwner)) {
            return reply.code(500).send('Permision denied!');
        }

        const { id } = req.params;
        const { password, cost, money, programs, administratorPhone } = req.body;

        const updatePass = !!password?.length;

        const company = await Company.findById(id);

        if (company) {
            if (usr.isOwner && updatePass) {
                await Company.updateOne({ _id: id }, {
                    password: await scryptHash(password),
                });
            } else if (usr.isRoot) {
                const progs = await Program.find();
                if (!progs) {
                    return reply.code(500).send('Programs not found!');
                }

                await Company.updateOne({ _id: id }, updatePass ? {
                    password: await scryptHash(password),
                    cost,
                    money: money > 0 ? company.money + money : 0,
                    programs: progs.filter(pr => programs.find(o => o === pr.id.toString())),
                    administratorPhone,
                } : {
                    cost,
                    money: money > 0 ? company.money + money : 0,
                    programs: progs.filter(pr => programs.find(o => o === pr.id)),
                    administratorPhone,
                });
            } else {
                req.log.error(`Can't update company ${id}`);
                return reply.code(500).send(`Can't update company ${id}`);
            }
            return reply.code(200).send({
                ...company._doc,
                id: company.id,
                _id: undefined,
                _v: undefined,
            });
        }
    } catch (err) {
        req.log.error(`company.update err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const deleteOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!usr.isRoot) {
            return reply.code(500).send('Permision denied!');
        }

        const { id } = req.params;

        const delUsers = await User.deleteMany({ companyId: id });
        if (delUsers) {
            req.log.warn(`Users for company ${id} deleted!`);
        }

        const delTerminals = await Terminal.deleteMany({ companyId: id });
        if (delTerminals) {
            req.log.warn(`Terminals for company ${id} deleted!`);
        }

        const delTermlogs = await Termlog.deleteMany({ companyId: id });
        if (delTermlogs) {
            req.log.warn(`Termlogs for company ${id} deleted!`);
        }

        const delOrders = await PayOrder.deleteMany({ companyId: id });
        if (delOrders) {
            req.log.warn(`Payorders for company ${id} deleted!`);
        }

        const del = await Company.findByIdAndDelete(id);
        if (del) {
            req.log.warn(`Company ${id} deleted!`);
            return reply.code(200).send({
                ...del._doc,
                id: del.id,
                password: undefined,
                _id: undefined,
                _v: undefined
            });
        }
        return reply.code(500).send('Cant delete company!');
    } catch (err) {
        req.log.error(`company.deleteOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}
