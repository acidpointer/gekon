import { FastifyReply, FastifyRequest } from 'fastify';
import fs from 'fs';
import jwt from 'jsonwebtoken';

import { scryptCompare } from '../../../utils/crypto';
import { GekonPermission } from '../../types';

import User from '../../../models/user.model';
import Company from '../../../models/company.model';


export const adminGet = async (req: FastifyRequest, reply: FastifyReply) => {
    return reply.sendFile('index.html', '/adminui');
}

export const adminAuthPost = async (req: FastifyRequest<{ Body: { username: string, password: string } }>, reply: FastifyReply) => {
    try {
        const { username, password } = req.body;
        const privkey = await fs.promises.readFile('/key/.priv.pem');

        if (username === 'admin' && password === process.env.ADMIN_PASSWORD) {
            const token = jwt.sign({ username, permissions: GekonPermission.ROOT }, privkey);
            return reply.code(200).send({ id: '-1', fullName: 'ROOT', username, token, permissions: ['ROOT'] });
        }

        const user = await User.findOne({ name: username });
        if (user) {
            const eq = await scryptCompare(user.password, password);
            if (eq) {
                const token = jwt.sign({ username }, privkey);
                return reply.code(200).send({ id: user.id, fullName: user.fullName, username, token, permissions: user.permissions });
            }
        }

        const company = await Company.findOne({ login: username });
        if (company) {
            const eq = await scryptCompare(company.password, password);
            if (eq) {
                const token = jwt.sign({ username }, privkey);
                return reply.code(200).send({ id: company.id, fullName: company.name, username, token, permissions: ['OWNER'] });
            }
        }
    } catch (err) {
        reply.log.error(`Authorization critical error: ${err}`);
    }
    return reply.code(500).send('Authorization failed!');
}