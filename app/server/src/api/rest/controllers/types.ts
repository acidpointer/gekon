export interface IGetListQuery {
    _end: string;
    _order: string;
    _sort: string;
    _start: string;
}