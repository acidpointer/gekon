// DEPRECATED!

import { FastifyReply, FastifyRequest } from 'fastify';

import { getRandomArrayElement } from '../../../utils/arrays';

const emoji = ['😎', '🥇', '🌚', '🐺', '🤘', '🤙', '😈', '😈', '🤖', '🤠', '🧙', '🌞'];

const fortunes = [
    'Счастливого пути!',
    'Продуктивного дня!',
    'У Вас всё получится!',
    'Приезжайте к нам снова!',
    'Хорошей мойки!',
    'Вы лучшие!'
];

const bg = [
    'rgb(26, 160, 66)',
    'rgb(120, 160, 26)',
    'rgb(160, 26, 149)',
    'rgb(26, 160, 115)',
    'rgb(160, 26, 104)',
    '#2A9D8F',
    '#F4A261',
    '#E63946',
    '#A8DADC',
    '#005F73',
    '#94D2BD',
    '#AE2012',
    '#4361EE',
    '#4CC9F0',
    '#F72585',
    '#7209B7',
    '#90BE6D',
    '#577590',
    '#40916C',
    '#74C69D',
    '#E09F3E',
    '#9E2A2B',
    '#E5383B',
    '#FF595E',
    '#8AC926',
    '#6A4C93',
    '#1982C4'
];



export const codePageGet = async (req: FastifyRequest<{ Params: { code: string }}>, reply: FastifyReply) => {
    try {
        const { code } = req.params;

        // const customer = await Customer.findOne({ code });
        // if (customer) {
        //     return reply.view('code', {
        //         title: getRandomArrayElement(emoji),
        //         code: customer.code,
        //         bg: getRandomArrayElement(bg),
        //         fortune: getRandomArrayElement(fortunes)
        //     });
        // }
        return reply.code(404).send('Not found');
    } catch (err) {
        req.log.error(`codepage.codePageGet err: ${err}`);
        return reply.code(500).send('Something wrong...');
    }
}