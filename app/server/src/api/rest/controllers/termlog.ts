import { FastifyReply, FastifyRequest } from 'fastify';

import Termlog from '../../../models/termlog.model';

import { IGetListQuery } from './types';
import { getUserContext } from '../../../utils';

export interface IGetTermlogListQuery extends IGetListQuery {
    terminal: string;
}

export const getList = async (req: FastifyRequest<{ Querystring: IGetTermlogListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!usr.isRoot) {
            return reply.code(500).send('Permission denied!');
        }

        const { _start, _end, _sort, _order, terminal } = req.query;

        let searchCriteria = {};
        if (terminal && terminal.length) {
            searchCriteria = { ...searchCriteria, terminal: { $regex: terminal, $options: 'i' } };
        }

        const logs = await Termlog.find(searchCriteria, null, { skip: Number(_start), limit: Number(_end), sort: { [_sort]: _order.toLowerCase() } });

        if (logs) {
            const count = await Termlog.countDocuments(searchCriteria);
            return reply.code(200).header('X-Total-Count', count).send(logs?.map(o => ({
                ...o._doc,
                id: o.id,
                _id: undefined,
                _v: undefined,
            })));
        }

        return reply.code(500).send('Terminal logs not found');
    } catch (err) {
        req.log.error(`termlog.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!usr.isRoot) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;
        const log = await Termlog.findById(id);
        if (log) {
            return reply.code(200).send({
                ...log._doc,
                _id: undefined,
                _v: undefined,
            });
        }
        
        return reply.code(500).send('Terminal log not found');
    } catch (err) {
        req.log.error(`termlog.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}
