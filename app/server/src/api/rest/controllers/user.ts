import { FastifyReply, FastifyRequest } from 'fastify';

import User from '../../../models/user.model';
import Company from '../../../models/company.model';

import { IGetListQuery } from './types';
import { GekonPermission } from '../../types';
import { scryptHash } from '../../../utils/crypto';
import { checkPermission, getUserContext } from '../../../utils';


export const getList = async (req: FastifyRequest<{ Querystring: IGetListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.USERS_LIST))) {
            return reply.code(500).send('Permission denied!');
        }

        const search = usr.isRoot ? {} : { companyId: usr.companyId };

        const { _start, _end, _sort, _order } = req.query;

        const users = await User.find(search, null, { skip: Number(_start), limit: Number(_end), sort: { [_sort]: _order.toLowerCase() } });

        if (users) {
            const count = await User.countDocuments(search);
            return reply.code(200).header('X-Total-Count', count).send(
                users.map(o => ({
                    ...o._doc,
                    id: o.id,
                    password: undefined,
                    _id: undefined,
                    _v: undefined,
                })));
        }
        return reply.code(500).send('Users not found');
    } catch (err) {
        req.log.error(`user.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || usr.isRoot || checkPermission(usr, GekonPermission.USERS_LIST))) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;

        const search = usr.isRoot ? { _id: id } : { _id: id, companyId: usr.companyId };

        const user = await User.findOne(search);
        if (user) {
            return reply.code(200).send({
                ...user._doc,
                id: user.id,
                password: undefined,
                _id: undefined,
                _v: undefined,
            });
        }
        return reply.code(500).send('User not found');
    } catch (err) {
        req.log.error(`user.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export interface IUserCreate {
    Body: {
        phone: string;
        name: string;
        role: string;
        fullName: string;
        password: string;
        details: string;
        permissions: string[];
    }
}

export const create = async (req: FastifyRequest<IUserCreate>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || checkPermission(usr, GekonPermission.USERS_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { phone, name, fullName, password, role, permissions, details } = req.body;

        const avail = await User.findOne({ name });
        if (avail || name === 'admin') {
            return reply.code(500).send('User already exist!');
        }

        const availCompanies = await Company.find({ login: name });
        if (availCompanies && availCompanies.length) {
            return reply.code(500).send('Wrong login selected!');    
        }

        const user = await User.create({
            phone,
            name,
            password: await scryptHash(password),
            fullName,
            role,
            companyId: usr.companyId,
            permissions,
            details,
        });

        if (user) {
            return reply.code(200).send({
                ...user._doc,
                id: user.id,
                password: undefined,
                _id: undefined,
                _v: undefined,
            });
        }
        return reply.code(500).send('Cant create user!');
    } catch (err) {
        req.log.error(`user.create err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export interface IUserUpdate {
    Params: {
        id: string;
    },
    Body: {
        phone: string;
        name: string;
        role: string;
        fullName: string;
        password: string;
        details: string;
        permissions: string[];
    }
}

export const update = async (req: FastifyRequest<IUserUpdate>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || checkPermission(usr, GekonPermission.USERS_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;
        const { phone, fullName, password, role, permissions, details } = req.body;

        const updatePass = !!password?.length;
        const updatePerms = usr.isOwner || (checkPermission(usr, GekonPermission.USERS_SET_PERMISSION) && checkPermission(usr, GekonPermission.USERS_UPDATE));

        const user = await User.findOne({ _id: id, companyId: usr.companyId });
        if (user) {
            await User.updateOne({ _id: id, companyId: usr.companyId }, {
                phone,
                fullName,
                password: updatePass ? await scryptHash(password) : user.password,
                role,
                permissions: updatePerms ? permissions : user.permissions,
                details,
            });
            return reply.code(200).send({
                ...user._doc,
                id: user.id,
                password: undefined,
                _id: undefined,
                _v: undefined,
            });
        }
    } catch (err) {
        req.log.error(`user.update err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const deleteOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner || checkPermission(usr, GekonPermission.USERS_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;

        const del = await User.findOneAndDelete({ _id: id, companyId: usr.companyId });
        if (del) {
            return reply.code(200).send({
                ...del._doc,
                id: del.id,
                password: undefined,
                _id: undefined,
                _v: undefined,
            });
        }
        return reply.code(500).send('Cant delete user');
    } catch (err) {
        req.log.error(`user.deleteOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}
