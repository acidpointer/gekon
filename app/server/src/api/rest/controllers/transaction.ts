import { FastifyReply, FastifyRequest } from 'fastify';

import Pay from '../../../models/payment.model';

import { getUserContext } from '../../../utils';

import { IGetListQuery } from './types';

export interface IGetTransactionListQuery extends IGetListQuery {
    payId: string;
    description: string;
    transaction_id: string;
    sender_phone: string;
    sender_card_mask2: string;
    sender_card_bank: string;
}

export const getList = async (req: FastifyRequest<{ Querystring: IGetTransactionListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isRoot)) {
            return reply.code(500).send('Permission denied!');
        }

        const { _start, _end, _sort, _order, payId, description, transaction_id, sender_phone, sender_card_mask2, sender_card_bank } = req.query;

        let search = {} as {
            payId?: object,
            description?: object,
            transaction_id?: object,
            sender_phone?: object,
            sender_card_mask2?: object,
            sender_card_bank?: object
        };

        if (payId && payId.length) {
            search = { ...search, payId: { $regex: payId, $options: 'i' } };
        }

        if (description && description.length) {
            search = { ...search, description: { $regex: description, $options: 'i' } };
        }

        if (transaction_id && transaction_id.length) {
            search = { ...search, transaction_id: { $regex: transaction_id, $options: 'i' } };
        }

        if (sender_phone && sender_phone.length) {
            search = { ...search, sender_phone: { $regex: sender_phone, $options: 'i' } };
        }

        if (sender_card_mask2 && sender_card_mask2.length) {
            search = { ...search, sender_card_mask2: { $regex: sender_card_mask2, $options: 'i' } };
        }

        if (sender_card_bank && sender_card_bank.length) {
            search = { ...search, sender_card_bank: { $regex: sender_card_bank, $options: 'i' } };
        }

        const pays = await Pay.find(search, null, { skip: Number(_start), limit: Number(_end), sort: { [_sort]: _order.toLowerCase() } });

        if (pays) {
            const count = await Pay.countDocuments(search);
            return reply.code(200).header('X-Total-Count', count).send(pays?.map(o => ({
                ...o._doc,
                id: o.id,
                _id: undefined,
                _v: undefined,
            })));
        }
    } catch (err) {
        req.log.error(`transaction.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isRoot)) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;

        const order = await Pay.findById(id);
        if (order) {
            return reply.code(200).send({
                ...order._doc,
                id: order.id,
                _id: undefined,
                _v: undefined,
            });
        }
    } catch (err) {
        req.log.error(`transaction.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}
