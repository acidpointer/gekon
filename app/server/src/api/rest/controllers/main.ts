import { FastifyReply, FastifyRequest } from 'fastify';

export const mainGet = async (req: FastifyRequest, reply: FastifyReply) => {
    return reply.view('main');
}