import { FastifyReply, FastifyRequest } from 'fastify';

import Program, { IPreStart, IProgram, IProgramICons, IProgramLocaleString, ModeButtonType, ModeGunType } from '../../../models/program.model';
import Company from '../../../models/company.model';

import { dynamicSort, numArrToStr, strToNumArr } from '../../../utils/arrays';

import { IGetListQuery } from './types';
import { checkPermission, getCompanyPrograms, getUserContext } from '../../../utils';
import { GekonPermission } from '../../types';
import { GekonServer } from '../../../app';

export const getList = async (req: FastifyRequest<{ Querystring: IGetListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isRoot || usr.isOwner || checkPermission(usr, GekonPermission.PROGRAMS_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const { _end, _order, _sort, _start } = req.query;

        const programs = await getCompanyPrograms(usr.isRoot ? undefined : usr.companyId);

        if (programs && programs.length) {
            return reply
                .header('X-Total-Count', programs.length)
                .code(200)
                .send(programs
                    .slice(Number(_start), Number(_end))
                    .sort(dynamicSort(_sort, _order))
                    .map(program => ({
                        ...program,
                        startCommand: numArrToStr(program.startCommand),
                        stopCommand: numArrToStr(program.stopCommand),
                    })));
        }
        return reply.code(500).send('Programs not found');
    } catch (err) {
        req.log.error(`programs.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isRoot || usr.isOwner || checkPermission(usr, GekonPermission.PROGRAMS_UPDATE))) {
            return reply.code(500).send('Permission denied!');
        }

        const programs = await getCompanyPrograms(usr.isRoot ? undefined : usr.companyId);
        if (programs) {
            const { id } = req.params;
            const program = programs.find(o => o.id.toString() === id);
            if (program) {
                return reply.code(200).send({
                    ...program,
                    startCommand: numArrToStr(program.startCommand),
                    stopCommand: numArrToStr(program.stopCommand),
                });
            }
            return reply.code(500).send(`Program ${id} not found!`);
        }
        return reply.code(500).send('Programs not found');
    } catch (err) {
        req.log.error(`programs.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export interface IProgramCreate {
    Body: {
        name: string;
        title: IProgramLocaleString;

        icons: IProgramICons;
        button: ModeButtonType;
        submodes: string[];

        description?: IProgramLocaleString;
        rec?: IProgramLocaleString;
        type?: ModeGunType;
        cost?: number;
        preStart?: IPreStart;
        startCommand: string;
        stopCommand: string;
    }
}

export const create = async (req: FastifyRequest<IProgramCreate>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isRoot)) {
            return reply.code(500).send('Permission denied!');
        }

        const {
            title, name, icons, button, submodes, description, rec, type, cost, preStart, startCommand, stopCommand
        } = req.body;

        const subPrograms = submodes ? await Program.find({ _id: submodes }) : [];
        const program = await Program.create({
            title,
            cost,
            name,
            icons,
            button,
            submodes: subPrograms,
            description,
            rec,
            type,
            preStart,
            startCommand: startCommand ? strToNumArr(startCommand) : [],
            stopCommand: stopCommand ? strToNumArr(stopCommand) : [],
        });

        if (program) {
            //@ts-ignore
            return reply.code(200).send({ ...program._doc, id: program._id });
        }
    } catch (err) {
        req.log.error(`programs.create err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
};

export interface IProgramUpdate {
    Params: {
        id: string;
    };
    Body: {
        name: string;
        title: IProgramLocaleString;
        icons: IProgramICons;
        button: ModeButtonType;
        submodes: string[];

        description?: IProgramLocaleString;
        rec?: IProgramLocaleString;
        type?: ModeGunType;
        cost?: number;
        preStart?: IPreStart;
        startCommand: string;
        stopCommand: string;
    }
}

export const update = async (req: FastifyRequest<IProgramUpdate>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);

        if (!(usr.isRoot || usr.isOwner)) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;
        const {
            title, name, icons, button, submodes, description, rec, type, cost, preStart, startCommand, stopCommand
        } = req.body;

        if (usr.isRoot) {
            const subPrograms = submodes ? await Program.find({ _id: submodes }) : [];
            const programs = await Program.updateOne({ _id: id }, {
                title,
                cost,
                name,
                icons,
                button,
                submodes: subPrograms,
                description,
                rec,
                type,
                preStart,
                startCommand: startCommand ? strToNumArr(startCommand) : [],
                stopCommand: stopCommand ? strToNumArr(stopCommand) : [],
            });

            if (programs) {
                return reply.code(200).send(programs);
            }
        } else if (usr.isOwner) {
            const companyId = usr.companyId;
            const company = await Company.findById(companyId);
            console.log('Programs up[date');
            if (company) {
                const progs = company.programs.map((p: IProgram) => {
                    if (p.id.toString() === id) {
                        return {
                            ...p,
                            cost,
                            title,
                            icons,
                            button,
                            description,
                            rec,
                            type,
                            preStart,
                        }
                    }
                    return p;
                });
                await Company.updateOne({ _id: companyId }, {
                    programs: progs,
                });
                GekonServer.instance.handler.restartAll();

                return reply.code(200).send(progs);
            }
        }
        return reply.code(500).send('Program not found!');
    } catch (err) {
        req.log.error(`programs.update err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const deleteOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!usr.isRoot) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;

        const del = await Program.findOneAndDelete({ id });
        if (del) {
            return reply.code(200).send({
                ...del,
                id: del._id,
                _id: undefined,
                _v: undefined,
            });
        }
        return reply.code(500).send('Cant delete terminal!');
    } catch (err) {
        req.log.error(`programs.deleteOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}