import { FastifyReply, FastifyRequest } from 'fastify';

import PayOrder from '../../../models/payorder.model';
import { getUserContext } from '../../../utils';

import { IGetListQuery } from './types';

export interface IGetOrdersListQuery extends IGetListQuery {
    orderId: string;
    payId: string;
    target: string;
}

export const getList = async (req: FastifyRequest<{ Querystring: IGetOrdersListQuery }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner)) {
            return reply.code(500).send('Permission denied!');
        }

        const { _start, _end, _sort, _order, orderId, payId, target } = req.query;

        let search = { companyId: usr.companyId } as { companyId?: string, orderId?: object, payId?: object, target?: object };

        if (orderId && orderId.length) {
            search = { ...search, orderId: { $regex: orderId, $options: 'i' } };
        }

        if (payId && payId.length) {
            search = { ...search, payId: { $regex: payId, $options: 'i' } };
        }

        if (target && target.length) {
            search = { ...search, target: { $regex: target, $options: 'i' } };
        }

        const orders = await PayOrder.find(search, null, { skip: Number(_start), limit: Number(_end), sort: { [_sort]: _order.toLowerCase() } });

        if (orders) {
            const count = await PayOrder.countDocuments(search);
            return reply.code(200).header('X-Total-Count', count).send(orders?.map(o => ({
                ...o._doc,
                id: o.id,
                _id: undefined,
                _v: undefined,
            })));
        }
    } catch (err) {
        req.log.error(`payorder.getList err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}

export const getOne = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    try {
        const usr = getUserContext(req);
        if (!(usr.isOwner)) {
            return reply.code(500).send('Permission denied!');
        }

        const { id } = req.params;

        const order = await PayOrder.findById(id);
        if (order) {
            return reply.code(200).send({
                ...order._doc,
                id: order.id,
                _id: undefined,
                _v: undefined,
            });
        }
    } catch (err) {
        req.log.error(`payorder.getOne err: ${err}`);
        return reply.code(500).send(`${err}`);
    }
}
