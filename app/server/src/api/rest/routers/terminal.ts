import { FastifyPluginCallback } from 'fastify';

import { getList, getOne, create, update, deleteOne } from '../controllers/terminal';

export const terminalRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/terminal:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/terminal/:id',
        preHandler,
        handler: getOne,
    });

    fastify.route({
        method: 'POST',
        url: '/terminal',
        preHandler,
        handler: create,
    });

    fastify.route({
        method: 'PUT',
        url: '/terminal/:id',
        preHandler,
        handler: update,
    });

    fastify.route({
        method: 'DELETE',
        url: '/terminal/:id',
        preHandler,
        handler: deleteOne,
    });

    done();
}