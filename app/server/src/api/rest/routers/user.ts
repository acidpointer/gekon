import { FastifyPluginCallback } from 'fastify';

import { getList, getOne, create, update, deleteOne } from '../controllers/user';

export const userRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/user:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/user/:id',
        preHandler,
        handler: getOne,
    });

    fastify.route({
        method: 'POST',
        url: '/user',
        preHandler,
        handler: create,
    });

    fastify.route({
        method: 'PUT',
        url: '/user/:id',
        preHandler,
        handler: update,
    });

    fastify.route({
        method: 'DELETE',
        url: '/user/:id',
        preHandler,
        handler: deleteOne,
    });

    done();
}