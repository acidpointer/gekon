import { FastifyPluginCallback } from 'fastify';

import { liqPayConfirm, payPage, payData, testUrl } from '../controllers/pay';

export const payRouter: FastifyPluginCallback = (fastify, opts, done) => {
    fastify.get('/pay/:token', payPage);
    fastify.get('/paytest', testUrl);
    fastify.post('/pd', payData);
    fastify.post('/liqpay', liqPayConfirm);
    done();
}