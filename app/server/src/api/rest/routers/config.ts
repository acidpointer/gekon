import { FastifyPluginCallback } from 'fastify';

import { getList, getOne, update } from '../controllers/config';

export const configRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/config:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/config/:id',
        preHandler,
        handler: getOne,
    });

    fastify.route({
        method: 'PUT',
        url: '/config/:id',
        preHandler,
        handler: update,
    });
    done();
}