import { FastifyPluginCallback } from 'fastify';

import { getList, getOne } from '../controllers/termlog';

export const termlogRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/termlog:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/termlog/:id',
        preHandler,
        handler: getOne,
    });

    done();
}