import { FastifyPluginCallback } from 'fastify';

import { getList, getOne } from '../controllers/payorder';

export const payOrderRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/payorder:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/payorder/:id',
        preHandler,
        handler: getOne,
    });

    done();
}