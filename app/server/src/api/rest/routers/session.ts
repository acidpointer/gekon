import { FastifyPluginCallback } from 'fastify';

import { getList, getOne } from '../controllers/session';

export const sessionRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/session:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/session/:id',
        preHandler,
        handler: getOne,
    });

    done();
}