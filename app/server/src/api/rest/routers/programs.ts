import { FastifyPluginCallback } from 'fastify';

import { getList, getOne, create, update, deleteOne } from '../controllers/programs';

export const programRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/programs:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/programs/:id',
        preHandler,
        handler: getOne,
    });

    fastify.route({
        method: 'POST',
        url: '/programs',
        preHandler,
        handler: create,
    });

    fastify.route({
        method: 'PUT',
        url: '/programs/:id',
        preHandler,
        handler: update,
    });

    fastify.route({
        method: 'DELETE',
        url: '/programs/:id',
        preHandler,
        handler: deleteOne,
    });

    done();
}