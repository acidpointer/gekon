import { FastifyPluginCallback } from 'fastify';

import { mainGet } from '../controllers/main';

export const mainRouter: FastifyPluginCallback = (fastify, opts, done) => {
    fastify.get('/', mainGet);
    done();
}