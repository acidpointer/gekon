import { FastifyPluginCallback } from 'fastify';

import { adminGet, adminAuthPost } from '../controllers/admin';

export const adminRouter: FastifyPluginCallback = (fastify, opts, done) => {
    fastify.route({
        method: 'GET',
        url: '*',
        preHandler: fastify.auth([
            //@ts-ignore
            fastify.allowAnonymous,
            fastify.verifyBearerAuth,
        ]),
        handler: adminGet,
    });
    fastify.route({
        method: 'POST',
        url: '/auth',
        preHandler: fastify.auth([
            //@ts-ignore
            fastify.allowAnonymous,
            fastify.verifyBearerAuth,
        ]),
        handler: adminAuthPost,
    });
    done();
}