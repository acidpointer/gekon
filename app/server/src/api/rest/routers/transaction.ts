import { FastifyPluginCallback } from 'fastify';

import { getList, getOne } from '../controllers/transaction';

export const transactionRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/transaction:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/transaction/:id',
        preHandler,
        handler: getOne,
    });

    done();
}