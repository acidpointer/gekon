import { FastifyPluginCallback } from 'fastify';

import { getList, getOne } from '../controllers/cash';

export const cashRouter: FastifyPluginCallback = (fastify, opts, done) => {
    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/cash:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/cash/:id',
        preHandler,
        handler: getOne,
    });

    done();
}