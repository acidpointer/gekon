import { FastifyPluginCallback } from 'fastify';

import { codePageGet } from '../controllers/codepage';

export const codepageRouter: FastifyPluginCallback = (fastify, opts, done) => {
    fastify.route({
        method: 'GET',
        url: '/u/:code',
        preHandler: fastify.auth([
            //@ts-ignore
            fastify.allowAnonymous,
            fastify.verifyBearerAuth,
        ]),
        handler: codePageGet,
    });
    done();
}