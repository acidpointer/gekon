import { FastifyPluginCallback } from 'fastify';

import { getList, getOne, create ,update, deleteOne } from '../controllers/company';

export const companyRouter: FastifyPluginCallback = (fastify, opts, done) => {

    const preHandler = fastify.verifyBearerAuth ? fastify.auth([
        fastify.verifyBearerAuth,
    ]) : undefined;

    if (!preHandler) return;

    fastify.route({
        method: 'GET',
        url: '/company:params',
        preHandler,
        handler: getList,
    });

    fastify.route({
        method: 'GET',
        url: '/company/:id',
        preHandler,
        handler: getOne,
    });

    fastify.route({
        method: 'POST',
        url: '/company',
        preHandler,
        handler: create,
    });

    fastify.route({
        method: 'PUT',
        url: '/company/:id',
        preHandler,
        handler: update,
    });

    fastify.route({
        method: 'DELETE',
        url: '/company/:id',
        preHandler,
        handler: deleteOne,
    });
    done();
}
