import { fastify, FastifyInstance, FastifyReply, FastifyRequest, FastifyServerOptions } from 'fastify';
import { IncomingMessage, Server, ServerResponse } from 'http';
import fs from 'fs';
import jwt from 'jsonwebtoken';

import fastifyAuth from 'fastify-auth';
import fastifyBearerAuth from 'fastify-bearer-auth';
import fastifyRateLimit from 'fastify-rate-limit';
import fastifyCors from 'fastify-cors';
import { fastifyRequestContextPlugin } from 'fastify-request-context';
import fastifyStatic from 'fastify-static';
import fastifyFormBody from 'fastify-formbody';
import pointOfView from 'point-of-view';
import * as eta from 'eta';

import logger from '../../logger';

import User from '../../models/user.model';
import Company from '../../models/company.model';

//import { fuckScannerRouter } from './routers/fuck-scanner';
import { mainRouter } from './routers/main';
import { adminRouter } from './routers/admin';
import { programRouter } from './routers/programs';
import { configRouter } from './routers/config';
import { terminalRouter } from './routers/terminal';
import { termlogRouter } from './routers/termlog';
import { sessionRouter } from './routers/session';
import { companyRouter } from './routers/company';
import { userRouter } from './routers/user';
import { payRouter } from './routers/pay';
import { codepageRouter } from './routers/codepage';
import { payOrderRouter } from './routers/payorder';
import { transactionRouter } from './routers/transaction';
import { cashRouter } from './routers/cash';

import redis from '../../redis';
import { setUserContext } from '../../utils';

// The main function for main fastify object!
// Inside this function, we can easily init all the neccessary middleware
// for fastify!
// Do not forget, all routers are middlewares too, so we should register
// it inside this function!
const build = (opts: FastifyServerOptions): FastifyInstance<Server, IncomingMessage, ServerResponse> => {
    const server = fastify(opts);

    // SUPER DUPER SECRET PRIVATE KEY
    // For JWT authorization
    const privKey = fs.readFileSync('/key/.priv.pem').toString('utf-8');

    // server.register(fastifyHelmet, {
    //     enableCSPNonces: true,
    //     contentSecurityPolicy: {
    //         directives: {
    //             defaultSrc: ["'self'"],

    //             scriptSrc: [
    //                 function (req, res: any) {
    //                     // "res" here is actually "reply.raw" in fastify
    //                     res.scriptNonce = crypto.randomBytes(16).toString('hex')
    //                 }
    //             ],
    //             styleSrc: [
    //                 function (req, res: any) {
    //                     // "res" here is actually "reply.raw" in fastify
    //                     res.styleNonce = crypto.randomBytes(16).toString('hex')
    //                 }
    //             ],
                

    //         }
    //     }
    // });

    server.register(fastifyRateLimit, {
        global: false,
        redis,
    });

    server.register(fastifyCors, {
        origin: ["https://www.liqpay.ua/api/3/checkout"]
    });

    // If we reach the rate limit,
    // the 429 error will be rised!
    // server.setErrorHandler((f, error, request, reply) => {
    //     if (reply.statusCode === 429) {
    //         return reply.send({
    //             ...error,
    //             error: 'Unhandled exception',
    //             message: 'Exception in internal logic',
    //         });
    //     }
    //     reply.send(error);
    // });

    server.register(fastifyRequestContextPlugin);
    server.register(fastifyAuth);

    // We authorize the user by Authorization http header
    server.register(fastifyBearerAuth, {
        keys: new Set(['']),
        bearerType: 'Bearer',
        addHook: false,

        // Wrong authorization responce!
        errorResponse: (err) => {
            logger.error(`Authorization err: ${err}`);
            return { error: 'Authorization error!' };
        },

        // Let's go!
        // Very long authorization function!
        // Here we also set the user context
        // User context allows to process user permissions or some flags
        // in ANY controller with FastifyRequest!
        auth: async (key: string, req: FastifyRequest): Promise<boolean> => {
            const verify = jwt.verify(key, privKey);
            let result = false;
            let username = 'unknown';
            let userId = null;
            let companyId = '';
            let permissions: string[] | null = null;
            let isRoot = false;
            let isOwner = false;

            if (typeof verify === 'object') {
                // 'admin' is the root's username
                // What is root?
                // By default, root allows to configure the core system
                // So, with root we can set some default configuration paramethers
                // Or default program's costs
                // But even root do not have access to secret companies data
                // Root can only create or delete companies, but do not modify
                // companies data!

                // BUT! Root allows to change the companys username or password
                // So, it is very dangerous to use root not only for configuring
                // the core system! So, do not use root!
                if (verify.username === 'admin') {
                    result = true;
                    permissions = verify.permissions;
                    username = verify.username;
                    isOwner = false;
                    isRoot = true;
                } else {
                    // Okay, we sure that user IS NOT ROOT!
                    // So we set the isRoot flag to false
                    // to prevent errors
                    isRoot = false;

                    // Now, we try to find user
                    const user = await User.findOne({ name: verify.username });
                    if (user) {
                        // Greaat! We found the user! Let's set variables
                        // and flags!
                        result = true;
                        username = user.name;
                        userId = user.id;
                        isOwner = false;
                        companyId = user.companyId;
                        permissions = user.permissions;
                    } else {
                        // Oh, we dont found the user!
                        // Maybe we have the company account?
                        // Let's search!
                        const company = await Company.findOne({ login: verify.username });
                        if (company) {
                            // Great! We found that!
                            result = true;
                            isOwner = true;
                            username = company.login;
                            companyId = company.id;
                        }
                    }
                }

                // If the result is true(authorization passed),
                // We now should set the user context with some data
                // For easily interaction, use the some utility functions
                if (result) setUserContext(req, {
                    isRoot,
                    isOwner,
                    permissions,
                    companyId,
                    username,
                    userId,
                });
            }
            return result;
        },
    });

    server.decorate('allowAnonymous', (req: FastifyRequest, reply: FastifyReply, done: any) => {
        // TODO: Slightly rework
        // if (req.headers.authorization) {
        //     return done(Error('not anonymous'));
        // }
        return done();
    });

    // Eta it's like the Jinja2 template language
    eta.configure({
        cache: false
    });

    // The great plugin for static templates!
    server.register(pointOfView, {
        engine: {
            eta: eta
        },
        root: '/view',
        viewExt: 'html',
    });

    server.register(fastifyStatic, {
        root: [
            '/adminui/static/',
            '/view/static',
        ],
        prefix: '/static/'
    });

    server.register(fastifyFormBody);

    // Control panel should have some prefix to access
    const adminPrefix = '/admin';

    // Custom routers should be here!
    const routes = () => {
        // WTF?
        // Some shitty scanners tries to scan our gret application!
        // Let's broke them with our special controller!
        //server.register(fuckScannerRouter);

        // The main router wit the blank page!
        // https://gekon.one/
        server.register(mainRouter);

        // Some control panel related routers:

        server.register(companyRouter, { prefix: adminPrefix });
        server.register(configRouter, { prefix: adminPrefix });
        server.register(programRouter, { prefix: adminPrefix });
        server.register(terminalRouter, { prefix: adminPrefix });
        server.register(termlogRouter, { prefix: adminPrefix });
        server.register(sessionRouter, { prefix: adminPrefix });
        server.register(userRouter, { prefix: adminPrefix });
        server.register(payOrderRouter, { prefix: adminPrefix });
        server.register(transactionRouter, { prefix: adminPrefix });
        server.register(cashRouter, { prefix: adminPrefix });

        // The payment system should have the router too!
        server.register(payRouter);

        server.register(codepageRouter);
    };

    server.register(adminRouter, { prefix: adminPrefix });
    server.after(routes);
    return server;
}

// Please, NEVER register any middlewares outside build() function!
// Very important thing!
const server: FastifyInstance<Server, IncomingMessage, ServerResponse> = build({ logger });

export default server;
