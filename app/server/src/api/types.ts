export interface IUserContextData {
    permissions: string[] | null;
    username: string;
    isOwner: boolean;
    isRoot: boolean;
    companyId: string;
    userId: string | null;
}

// WARNING!
// After create new permission,
// update the permissions enum in user.model.ts !!!!!!!!!!
export enum GekonPermission {
    ROOT = 'ROOT',

    USERS_LIST = 'USERS_LIST',
    USERS_CREATE = 'USERS_CREATE',
    USERS_UPDATE = 'USERS_UPDATE',
    USERS_SET_PERMISSION = 'USERS_SET_PERMISSION',
    USERS_DELETE = 'USERS_DELETE',

    COMPANIES_LIST = 'COMPANIES_LIST',
    COMPANIES_CREATE = 'COMPANIES_CREATE',
    COMPANIES_UPDATE = 'COMPANIES_UPDATE',
    COMPANIES_DELETE = 'COMPANIES_DELETE',

    TERMINAL_LIST = 'TERMINAL_LIST',
    TERMINAL_CREATE = 'TERMINAL_CREATE',
    TERMINAL_UPDATE = 'TERMINAL_UPDATE',
    TERMINAL_DELETE = 'TERMINAL_DELETE',

    CUSTOMERS_LIST = 'CUSTOMERS_LIST',
    CUSTOMERS_CREATE = 'CUSTOMERS_CREATE',
    CUSTOMERS_UPDATE = 'CUSTOMERS_UPDATE',
    
    CONFIG_LIST = 'CONFIG_LIST',
    CONFIG_UPDATE = 'CONFIG_UPDATE',

    SESSION_LIST = 'SESSION_LIST',
    
    PROGRAMS_LIST = 'PROGRAMS_LIST',
    PROGRAMS_UPDATE = 'PROGRAMS_UPDATE',

    ALLOW_INSPECTION = 'ALLOW_INSPECTION',
}