import { nanoid } from 'nanoid';

import Single from './models/single.model';
import { IConfig, IParameter, IProgramGroup } from './models/types';

import logger from './logger';

export const initWashtermDefaultPrograms = async () => {

    const programs = [
        {
            id: nanoid(),
            title_ru: 'Вода',
            title_ua: 'Вода',
            title_en: 'Water',
            icon_start: 'green',
            color: 'blue',
            shortName: 'default_water',
            index: 1,
            cost: 10,
            //type: 'washterm',
            group: 'waters',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Вода турбо',
            title_ua: 'Вода турбо',
            title_en: 'Turbo water',
            icon_start: 'green',
            color: 'blue',
            shortName: 'turbo_water',
            index: 2,
            tier: 1,
            cost: 13,
            //type: 'washterm',
            group: 'waters',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Теплая вода',
            title_ua: 'Тепла вода',
            title_en: 'Warm water',
            icon_start: 'yellow',
            color: 'blue',
            shortName: 'warm_water',
            index: 3,
            tier: 0,
            cost: 15,
            //type: 'washterm',
            group: 'waters',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x04],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Вода + Шампунь',
            title_ua: 'Вода + Шампунь',
            title_en: 'Water + Shampoo',
            icon_start: 'green',
            color: 'blue',
            shortName: 'shampoo_water',
            index: 4,
            tier: 0,
            cost: 15,
            //type: 'washterm',
            group: 'waters',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Белая пена',
            title_ua: 'Бiла пiна',
            title_en: 'White foam',
            icon_start: 'green',
            color: 'red',
            shortName: 'white_foam',
            index: 5,
            tier: 1,
            cost: 21,
            //type: 'washterm',
            group: 'foams',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x05],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Розовая пена',
            title_ua: 'Розова пiна',
            title_en: 'Pink foam',
            icon_start: 'pink',
            color: 'red',
            shortName: 'pink_foam',
            index: 6,
            tier: 1,
            cost: 21,
            //type: 'washterm',
            group: 'foams',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x05],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Воск',
            title_ua: 'Вiск',
            title_en: 'Wax',
            icon_start: 'green',
            color: 'blue',
            shortName: 'wax',
            index: 7,
            tier: 1,
            cost: 33,
            //type: 'washterm',
            group: 'empty',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x06],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Осмос',
            title_ua: 'Осмос',
            title_en: 'Osmosis',
            icon_start: 'green',
            color: 'blue',
            shortName: 'osmosis',
            index: 8,
            tier: 2,
            cost: 34,
            //type: 'washterm',
            group: 'empty',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x07],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x08],
        },
        //
        {
            id: nanoid(),
            title_ru: 'Пылесос',
            title_ua: 'Пилосос',
            title_en: 'Hoover',
            icon_start: 'green',
            color: 'blue',
            shortName: 'hoover',
            index: 9,
            tier: 2,
            cost: 34,
            //type: 'washterm',
            group: 'empty',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x07],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Воздух',
            title_ua: 'Повiтря',
            title_en: 'Air',
            icon_start: 'green',
            color: 'blue',
            shortName: 'air',
            index: 10,
            tier: 2,
            cost: 34,
            //type: 'washterm',
            group: 'empty',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x07],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x08],
        },
        {
            id: nanoid(),
            title_ru: 'Сушка',
            title_ua: 'Сушка',
            title_en: 'Drying',
            icon_start: 'green',
            color: 'blue',
            shortName: 'drying',
            index: 11,
            tier: 2,
            cost: 34,
            //type: 'washterm',
            group: 'empty',
            startCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x07],
            stopCommand: [0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x08],
        }
    ];

    const programGroups: IProgramGroup[] = [
        {
            id: nanoid(),
            enabled: true,
            shortName: 'waters',
            title_ru: 'Вода',
            title_ua: 'Вода',
            title_en: 'Water',
        },
        {
            id: nanoid(),
            enabled: true,
            shortName: 'foams',
            title_ru: 'Пена',
            title_ua: 'Пiна',
            title_en: 'Foams',
        },
        {
            id: nanoid(),
            enabled: true,
            shortName: 'hoov',
            title_ru: 'Пылесос',
            title_ua: 'Пилосос',
            title_en: 'Hoover',
        },
    ];

    const availSingleProgs = await Single.findOne({ name: 'programs' });

    const availSingleProgGroups = await Single.findOne({ name: 'program_groups' });

    if (availSingleProgGroups) return;

    if (availSingleProgs) return;

    logger.warn('Programs not found in db! Default values will add!');
    await Single.create({
        name: 'programs',
        item: { programs },
    });

    await Single.create({
        name: 'program_groups',
        item: { programGroups },
    });
}

export const initDefaultConfiguration = async () => {

    const defaultParams: IParameter[] = [
        {
            id: nanoid(),
            index: 1,
            name: 'Бонусы за регистрацию',
            description: 'Количество начисляемых при регистрации бонусов',
            shortName: 'bonus_reg_gift',
            value: 10,
            category: 'loyalty',
        },
        {
            id: nanoid(),
            index: 2,
            name: 'Деньги за регистрацию',
            description: 'Количество начисляемых при регистрации кредитов',
            shortName: 'money_reg_gift',
            value: 0,
            category: 'loyalty',
        },
        {
            id: nanoid(),
            index: 3,
            name: 'Коэфициент начисления бонусов от суммы пополнения',
            description: '[коэф] * [сумма_пополнения]',
            shortName: 'bonus_coef',
            value: 0.1,
            category: 'loyalty',
        },
        {
            id: nanoid(),
            index: 4,
            name: 'N мойка',
            description: 'Через какое кол-во моек начислять бонусы',
            shortName: 'gift_interval',
            value: 5,
            category: 'loyalty',
        },
        {
            id: nanoid(),
            index: 5,
            name: 'Бонусы за каждую N мойку',
            description: 'Подарочные бонусы каждую N мойку',
            shortName: 'bonus_visit_gift',
            value: 20,
            category: 'loyalty',
        },
        {
            id: nanoid(),
            index: 6,
            name: 'СМС уведомления',
            description: 'Разрешить отправку СМС сообщений? В противном случае будет отправлено в канал бота',
            shortName: 'sms_allow',
            value: 0,
            category: 'system'
        },
    ];

    try {
        const availConfig = await Single.findOne({ name: 'config' });

        if (availConfig) {
            const c = availConfig.item as IConfig;

            let params = [...c.parameters];

            let recreate = false;

            if (params.length !== defaultParams.length) {
                recreate = true;
            } else {
                for (let i = 0, len = defaultParams.length; i < len; i++) {
                    if (c.parameters[i].shortName !== defaultParams[i].shortName) {
                        logger.warn(`${c.parameters[i].shortName} !== [default] ${defaultParams[i].shortName} and will be (re)created!`);
                        if (params[i]) {
                            params[i] = defaultParams[i];
                        } else {
                            params.push(defaultParams[i]);
                        }
                        recreate = true;
                    }
                }
            }

            if (recreate) {
                await Single.deleteOne({ name: 'config' });
                await Single.create({
                    name: 'config',
                    item: { parameters: params },
                });
            }
            return;
        };
    } catch (err) {
        logger.error(`initDefaultConfiguration Err: ${err}`);
    }

    logger.warn('Config not found in db! Default will add!');
    await Single.create({
        name: 'config',
        item: { parameters: defaultParams },
    });
}