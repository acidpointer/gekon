import { FastifyRequest } from 'fastify';

import Single from './models/single.model';
import Company from './models/company.model';
import Terminal from './models/terminal.model';
import Program, { IProgram, IProgramBase } from './models/program.model';

import User, { IUser } from './models/user.model';

import redis from './redis';

import { IConfig, IParameter, } from './models/types';

import { GekonPermission, IUserContextData } from './api/types';

export const setUserContext = (req: FastifyRequest, usr: IUserContextData) => {
    req.requestContext.set('user', usr);
}

export const getUserContext = (req: FastifyRequest): IUserContextData => {
    return req.requestContext.get('user');
}

export const isOwner = (ctx: IUserContextData): boolean => {
    const { isOwner, isRoot, userId, username } = ctx;
    return isOwner && !isRoot && !!userId?.length && !!username?.length;
}

export const checkPermission = (ctx: IUserContextData, permission: GekonPermission): boolean => {
    const { permissions } = ctx;
    return ctx?.isRoot || !!(permissions?.find(o => o === permission));
}

export const getUser = async (req: FastifyRequest): Promise<IUser | null> => {
    const { username } = getUserContext(req);
    const user = await User.findOne({ name: username });
    return user;
}

export const getCompanyPrograms = async (companyId?: string): Promise<IProgram[]> => {
    if (companyId) {
        const company = await Company.findById(companyId);
        if (company) {
            return company.programs.map(o => o._id ? ({ ...o, id: o._id}) : o);
        }
    }
    const progs = await Program.find();
    return progs.map((o: any) => ({ ...o._doc, id: o._id }));
}

export const getTerminalPrograms = async (terminalId: string, lang: string) => {
    const terminal = await Terminal.findById(terminalId);
    if (terminal) {
        console.log('Terminal found!', terminal.id);
        console.log('Terminal programs:', terminal.programs);
        const company = await Company.findById(terminal.companyId);
        if (company) {
            console.log('Company programs:', company.programs);
            const pr = company.programs.filter((pr: any) => terminal.programs.includes(pr._id.toString()));
            console.log('Programs:', pr);
            return pr.map((p: any) => {
                const submodes = p.submodes.map((s: string) => {
                    const prog = company.programs.find((prog: any) => prog._id ? prog?._id.toString() === s?.toString() : prog?.id.toString() === s?.toString());
                    if (prog) {
                        return {
                            ...prog,
                            id: prog?._id ? prog?._id.toString() : prog?.id?.toString(),
                            title: prog?.title[lang] ?? 'unset_title',
                            rec: prog?.rec[lang] ?? 'unset_rec',
                            description: prog?.description[lang] ?? 'unset_descr',
                        }
                    }
                });
                return {
                    ...p,
                    submodes: submodes.length > 0 ? submodes[0]?.id ? submodes : [] : [],
                    id: p?._id,
                    title: p?.title[lang] ?? 'unset_title',
                    rec: p?.rec[lang] ?? 'unset_rec',
                    description: p?.description[lang] ?? 'unset_descr'
                };
            });
        }
    }
    return [];
}

export const getConfig = async (companyId?: string): Promise<IParameter[] | null> => {
    let config: IParameter[] | null = null;
    if (companyId) {
        const comp = await Company.findById(companyId);
        config = comp?.config && comp?.config.length ? comp.config : null;
    } else {
        const key = 'config';
        if (await redis.exists(key)) {
            const avail = await redis.get(key)
            if (avail) {
                config = JSON.parse(avail) as IParameter[];
            }
        } else {
            config = await cacheConfig();
        }
    }
    return config;
}

export const cacheConfig = async (lifetime: number = 60): Promise<IParameter[] | null> => {
    let config: IParameter[] | null = null;
    const key = 'config';
    const single = await Single.findOne({ name: 'config' });
    if (single) {
        const paramsItem = (single.item as IConfig);
        config = paramsItem.parameters;
        if (config) {
            await redis.set(key, JSON.stringify(config));
            await redis.expire(key, lifetime);
        }
    }
    return config;
}

export const getConfigValue = async (parameter: string, company?: string): Promise<number> => {
    const cfg = await getConfig(company);
    const f = cfg?.find(o => o.shortName === parameter)?.value;
    const val = f ? Number(f) : 0;
    return val >= 0 ? val : 0;
}