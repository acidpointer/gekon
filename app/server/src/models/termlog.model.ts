import { model, Model, Document, Schema } from 'mongoose';
import { nanoid } from 'nanoid';


export interface ITerminalLog {
    level: string;
    section: string;
    message: string;
    terminal: string;
    timestamp: Date;
}

// Methods
export interface ITerminalLogDocument extends ITerminalLog, Document {
    _doc: ITerminalLog;
}

// Statics
export interface ITerminalLogModel extends Model<ITerminalLogDocument> {
}

const TerminalLogSchema = new Schema<ITerminalLogDocument, ITerminalLogModel>({
    _id: {
        type: String,
        default: () => nanoid(30),
    },
    level: {
        type: String,
        required: true,
    },
    section: {
        type: String,
        required: true,
    },
    message: {
        type: String,
        required: true,
    },
    terminal: {
        type: String,
        required: true,
    },
    timestamp: {
        type: Date,
        required: true,
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default model('TerminalLog', TerminalLogSchema);