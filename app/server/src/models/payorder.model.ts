import { model, Model, Schema, Document } from 'mongoose';
import { nanoid, customAlphabet } from 'nanoid';

export interface IPayOrder {
    orderId: string;
    payId: string;
    amount: number;
    action: string;
    currency: string;
    target: string;
    type: string;
    companyId: string;
}

// Methods
export interface IPayOrderDocument extends IPayOrder, Document {
    _doc: IPayOrder;
}

// Statics
export interface IPayOrderModel extends Model<IPayOrderDocument> {
}

const rand = customAlphabet('QWERTYUIOPZXC0123456789', 33);

const PayOrderSchema = new Schema<IPayOrderDocument, IPayOrderModel>({
    _id: {
        type: String,
        default: () => nanoid(30),
    },
    orderId: {
        type: String,
        required: true,
    },
    payId: {
        type: String,
        required: true,
    },
    amount: {
        type: Number,
        default: 0,
        min: 0,
        required: true,
    },
    action: {
        type: String,
        required: true,
        enum: {
            values: ['pay', 'payqr', 'paycard', 'paybank'],
            message: '{VALUE} is not supported'
        }
    },
    currency: {
        type: String,
        required: true,
        enum: {
            values: ['USD', 'EUR', 'UAH'],
            message: '{VALUE} is not supported'
        }
    },
    type: {
        type: String,
        required: true,
        enum: {
            values: ['terminal', 'customer'],
            message: '{VALUE} is not supported'
        }
    },
    target: {
        type: String,
        required: true,
    },
    companyId: {
        type: String,
        required: true,
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default model('PayOrder', PayOrderSchema);