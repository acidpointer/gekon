import { model, Model, Document, Schema } from 'mongoose';
import { isValidPhoneNumber } from 'libphonenumber-js';
import { nanoid } from 'nanoid';

export interface IUser {
    phone: string;
    name: string;
    fullName: string;
    details: string;
    password: string;
    role: string;
    permissions: string[];
    companyId: string;
    loginsCount: number;
}

// Methods
export interface IUserDocument extends IUser, Document {
    _doc: IUser;
    countLogin: () => Promise<void>;
}

// Statics
export interface IUserModel extends Model<IUserDocument> {}

const UserSchema = new Schema<IUserDocument, IUserModel>({
    _id: {
        type: String,
        default: () => nanoid(30),
    },
    phone: {
        type: String,
        unique: true,
        dropDups: true,
        required: [true, 'User phone number required!'],
        validate: {
            validator: function(phone: string) {
                return isValidPhoneNumber(phone);
            },
            message: props => `${props.value} is not a valid phone number!`
        }
    },
    password: {
        type: String,
        required: true,
        validate: {
            validator: function(pass: string) {
                return /^.{4,}$/.test(pass);
            },
            message: props => `${props.value} min length should be 4!`
        }
    },
    name: {
        type: String,
        required: true,
        validate: {
            validator: function(name: string) {
                return /^[a-zA-Z0-9].{3,14}$/.test(name) && name !== 'admin';
            },
            message: props => `${props.value} Should contain only letters and numbers with min length 4 and max length 15!`
        }
    },
    fullName: {
        type: String,
        required: true,
    },
    permissions: [{
        type: String,
        required: true,
    }],
    details: {
        type: String,
        default: '',
        required: false,
    },
    role: {
        type: String,
        required: true,
    },
    companyId: {
        type: String,
        required: true,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

UserSchema.methods.countLogin = async function () {
    this.loginsCount = this.loginsCount + 1;
    return this.save();
}

export default model('User', UserSchema);
