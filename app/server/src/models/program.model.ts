import { model, Schema } from 'mongoose';
import { water } from '../icons.json';

export enum ModeButtonType {
    Default = 'default',
    White = 'white',
    Pink = 'pink',
    Yellow = 'yellow'
}

export enum ModeGunType {
    Red = 'red',
    Blue = 'blue'
}

export interface IProgramLocaleString {
    ua: string;
    en: string;
    ru: string;
}

export interface IProgramICons {
    modes: string;
    start: string;
}

export interface IPreStart {
    title: string;
    titleColor: string;
    icon: string;
}

export interface IProgramBase {
    id: string;
    name: string;
    title: IProgramLocaleString;
    icons: IProgramICons;
    enabled: boolean;
    button: ModeButtonType;
    submodes: IProgram[] | null;
}

export interface IProgram extends IProgramBase {
    description: IProgramLocaleString;
    rec: IProgramLocaleString;
    type: ModeGunType;
    selected: boolean;
    cost: number;
    balanceTime: number;
    bonusTime: number;
    preStart: IPreStart;
    startCommand: number[];
    stopCommand: number[];
}


const programSchema = new Schema<IProgram>({
    name: {
        type: String,
        default: 'Unnamed'
    },
    title: {
        type: Object,
        default: {
            ua: 'name_locale_ua_not_set',
            en: 'name_locale_en_not_set',
            ru: 'name_locale_ru_not_set',
        }
    },
    description: {
        type: Object,
        default: {
            ua: 'description_locale_ua_not_set',
            en: 'description_locale_en_not_set',
            ru: 'description_locale_ru_not_set',
        }
    },
    rec: {
        type: Object,
        default: {
            ua: 'rec_locale_ua_not_set',
            en: 'rec_locale_en_not_set',
            ru: 'rec_locale_ru_not_set',
        }
    },
    button: {
        type: String,
        defalut: ModeButtonType.Default,
    },
    type: {
        type: String,
        default: ModeGunType.Red,
    },
    enabled: {
        type: Boolean,
        default: true,
    },
    selected: {
        type: Boolean,
        default: false,
    },
    cost: {
        type: Number,
        default: 0,
        min: 0,
        max: 10_000,
    },
    balanceTime: {
        type: Number,
        default: 0,
        min: 0,
    },
    bonusTime: {
        type: Number,
        default: 0,
        min: 0,
    },
    icons: {
        type: Object,
        default: {
            modes: water,
            start: water,
        }
    },
    preStart: {
        type: Object,
        default: {
            title: '',
            titleColor: '',
            icon: '',
        }
    },
    startCommand: {
        type: [Number],
        default: [0, 0, 0, 0, 0, 0, 0, 0],
    },
    stopCommand: {
        type: [Number],
        default: [0, 0, 0, 0, 0, 0, 0, 0],
    },
    submodes: [{
        type: Schema.Types.ObjectId,
        ref: 'Program',
    }]
});

export default model('Program', programSchema);