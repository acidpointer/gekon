import { model, Model, Document, Schema } from 'mongoose';
import { nanoid, customAlphabet } from 'nanoid';

export interface IPayment {
    payId: string;
    amount: number;
    action: string;
    status: string;
    description: string;
    sender_phone?: string;
    sender_card_mask2: string;
    sender_card_bank: string;
    transaction_id: string;
    currency: string;
}

// Methods
export interface IPaymentDocument extends IPayment, Document {
    _doc: IPayment;
}

// Statics
export interface IPaymentModel extends Model<IPaymentDocument> {
}

const rand = customAlphabet('QWERTYUIOPZXC0123456789', 33);

const PaymentSchema = new Schema<IPaymentDocument, IPaymentModel>({
    _id: {
        type: String,
        default: () => nanoid(30),
    },
    payId: {
        type: String,
        default: () => rand(),
    },
    amount: {
        type: Number,
        default: 0,
        min: 0,
        required: true,
    },
    action: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    sender_phone: {
        type: String,
    },
    sender_card_mask2: {
        type: String,
        required: true,
    },
    sender_card_bank: {
        type: String,
        required: true,
    },
    transaction_id: {
        type: String,
        required: true,
    },
    currency: {
        type: String,
        required: true,
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default model('Payment', PaymentSchema);