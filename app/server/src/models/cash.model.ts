import { model, Model, Document, Schema } from 'mongoose';
import { nanoid, customAlphabet } from 'nanoid';

export interface ICash {
    cashId: string;
    terminal: string;
    companyId: string;
    amount: number;
}

// Methods
export interface ICashDocument extends ICash, Document {
    _doc: ICash;
}

// Statics
export interface ICashModel extends Model<ICashDocument> {
}

const rand = customAlphabet('QWERTYUIOPZXC0123456789', 33);

const CashSchema = new Schema<ICashDocument, ICashModel>({
    _id: {
        type: String,
        default: () => nanoid(30),
    },
    cashId: {
        type: String,
        default: () => rand(),
    },
    terminal: {
        type: String,
        required: true,
    },
    companyId: {
        type: String,
        required: true,
    },
    amount: {
        type: Number,
        required: true,
        min: 0,
        max: 10_000,
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default model('Cash', CashSchema);