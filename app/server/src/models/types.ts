import { Program } from '../rpc/schema/terminal_types';
import { ISingleItem } from './single.model';

export interface IProgramGroup {
    id: string;
    shortName: string;
    title_ru: string;
    title_ua: string;
    title_en: string;
    enabled: boolean;
}

export interface IPrograms extends ISingleItem {
    programs: Program[];
}

export interface IProgramGroups extends ISingleItem {
    programGroups: IProgramGroups[];
}

export interface IParameter {
    id: string;
    index: number;
    name: string;
    description: string;
    shortName: string;
    value: number;
    category: string; //'loyalty' | 'system' | 'mailing'
}

export interface IConfig extends ISingleItem {
    parameters: IParameter[];
}