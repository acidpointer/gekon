import { model, Model, Document, Schema } from 'mongoose';
import { nanoid } from 'nanoid';
import { IProgram } from './program.model';
import { IParameter, IProgramGroup } from './types';

export interface ICompany {
    name: string;
    login: string;
    password: string;
    active: boolean;
    cost: number;
    money: number;
    administratorPhone: string;
    programs: IProgram[];
    programGroups: IProgramGroup[];
    config: IParameter[];
}

// Methods
export interface ICompanyDocument extends ICompany, Document {
    _doc: ICompany;
}

// Statics
export interface ICompanyModel extends Model<ICompanyDocument> {
}

const CompanySchema = new Schema<ICompanyDocument, ICompanyModel>({
    _id: {
        type: String,
        default: () => nanoid(30),
    },
    name: {
        type: String,
        required: true,
    },
    login: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    active: {
        type: Boolean,
        required: true,
        default: false,
    },
    administratorPhone: {
        type: String,
        default: '',
    },
    programs: [{
        type: Object,
        required: true,
    }],
    programGroups: [{
        type: Object,
        required: true,
    }],
    config: [{
        type: Object,
        required: true,
    }],
    money: {
        type: Number,
        required: true,
    },
    cost: {
        type: Number,
        required: true,
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default model('Company', CompanySchema);
