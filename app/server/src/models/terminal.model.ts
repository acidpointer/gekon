import { model, Model, Document, Schema } from 'mongoose';
import { nanoid } from 'nanoid';
import { Lang } from '../rpc/schema/terminal_types';
import { IProgram } from './program.model';

export interface ITerminal {
    name: string;
    password: string;
    money: number;
    enabled: boolean;
    lastTopUp: Date;
    lastLogin: Date;
    loginsCount: number;
    inspectionInterval: number;
    lastInspection: Date;
    inspectionCount: number;
    totalWorkHours: number;
    currentWorkHours: number;
    companyId: string;
    lang: Lang;
    programs: string[];

    // Bill validator
    bvBanknots: number;
    bvCapacity: number;
}

// Methods
export interface ITerminalDocument extends ITerminal, Document {
    _doc: ITerminal;
}

// Statics
export interface ITerminalModel extends Model<ITerminalDocument> {
}

const TerminalSchema = new Schema<ITerminalDocument, ITerminalModel>({
    _id: {
        type: String,
        default: () => nanoid(30),
    },
    name: {
        type: String,
        unique: true,
        required: true,
        validate: {
            validator: function (name: string) {
                return /^[a-zA-Z0-9].{4,14}$/.test(name);
            },
            message: props => `${props.value} Should contain only letters and numbers with min length 5 and max length 15!`
        }
    },
    password: {
        type: String,
        required: true,
        validate: {
            validator: function (pass: string) {
                return /^.{4,}$/.test(pass);
            },
            message: props => `${props.value} min length should be 4!`
        }
    },
    lang: {
        type: Number,
        default: Lang.UA,
    },
    money: {
        type: Number,
        required: true,
        default: 0,
    },
    enabled: {
        type: Boolean,
        required: true,
        default: false,
    },
    lastTopUp: {
        type: Date,
    },
    lastLogin: {
        type: Date,
    },
    loginsCount: {
        type: Number,
        default: 0,
    },
    inspectionInterval: {
        type: Number,
        required: true,
    },
    inspectionCount: {
        type: Number,
        required: true,
        default: 0,
        min: 0,
    },
    currentWorkHours: {
        type: Number,
        default: 0,
        min: 0,
        required: true,
    },
    totalWorkHours: {
        type: Number,
        default: 0,
        min: 0,
        required: true,
    },
    lastInspection: {
        type: Date,
        required: false,
    },
    companyId: {
        type: String,
        required: true,
    },
    programs: [{
        type: Object,
        required: true,
    }],
    bvBanknots: {
        type: Number,
        default: 0,
        min: 0,
        max: 10_000,
    },
    bvCapacity: {
        type: Number,
        default: 0,
        min: 0,
        max: 10_000,
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default model('Terminal', TerminalSchema);
