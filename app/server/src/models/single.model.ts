import { model, Model, Document, Schema } from 'mongoose';
import { nanoid } from 'nanoid';

export interface ISingleItem {
    name: string;
}

export interface ISingle {
    name: string;
    item: ISingleItem;
}

// Methods
export interface ISingleDocument extends ISingle, Document { }

// Statics
export interface ISingleModel extends Model<ISingleDocument> {}

const SingleSchema = new Schema<ISingleDocument, ISingleModel>({
    _id: {
        type: String,
        default: () => nanoid(30),
    },
    name: {
        type: String,
        required: true,
    },
    item: {
        type: Object,
        required: true,
    }
});

export default model('Single', SingleSchema);