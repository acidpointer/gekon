import { createServer, TBinaryProtocol, TBufferedTransport } from 'thrift';
import * as http from 'http';
import * as tls from 'tls';

import TerminalSchema from './schema/Terminal';

import TerminalHandler from '../terminal';

export class RPCServer {

    #terminalServer: http.Server | tls.Server;
    #terminalHandler: TerminalHandler;

    constructor(handler: TerminalHandler) {
        this.#terminalHandler = handler;
        this.#terminalServer = createServer(TerminalSchema, this.#terminalHandler, {
            transport: TBufferedTransport,
            protocol: TBinaryProtocol,
        });
    }

    public start(host = 'gekon', port = 9897): Promise<void> {
        return new Promise((resolve, reject) => {
            this.#terminalServer.listen(port, host);

            this.#terminalServer.on('listening', () => {
                resolve();
            });

            this.#terminalServer.on('clientError', ()=> {
                console.log('client_err');
            })

            // this.#terminalServer.on('close', () => {
            //     logger.warn('[RPC] Server closed!');
            // });

            this.#terminalServer.once('error', (err: Error) => {
                reject(err);
            });
        });
    }

    public stop(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.#terminalServer.close((err: Error) => {
                if (!err) {
                    resolve();
                    return;
                };
                reject(err);
            });
        })
    }

    public get server(): http.Server | tls.Server {
        return this.#terminalServer;
    }
}