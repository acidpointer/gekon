import { getConfigValue } from '../utils';
import logger from '../logger';
import { Tg } from './bot/telegram';
import OmnicellClient from './sms/omnicell';
import { ISmsApi } from './sms/types';

export default class MailingsManager {
    private bot: Tg;
    private smsApi: ISmsApi;

    constructor() {
        const tgToken = process.env.TG_TOKEN ? process.env.TG_TOKEN : '';
        const tgChanErr = process.env.TG_CHANNEL_ERROR ? process.env.TG_CHANNEL_ERROR : '';
        const tgChanWarn = process.env.TG_CHANNEL_WARNING ? process.env.TG_CHANNEL_WARNING : '';
        const tgChanNotify = process.env.TG_CHANNEL_NOTIFY ? process.env.TG_CHANNEL_NOTIFY : '';
        const tgChanUsrNews = process.env.TG_CHANNEL_USERNEWS ? process.env.TG_CHANNEL_USERNEWS : '';
        const tgChanAdmNews = process.env.TG_CHANNEL_ADMINNEWS ? process.env.TG_CHANNEL_ADMINNEWS : '';

        this.bot = new Tg(tgToken, {
            error: tgChanErr,
            warn: tgChanWarn,
            notify: tgChanNotify,
            userNews: tgChanUsrNews,
            adminNews: tgChanAdmNews,
        });

        const omnicellLogin = process.env.OMNICELL_LOGIN ? process.env.OMNICELL_LOGIN : '';
        const omnicellPass = process.env.OMNICELL_PASSWORD ? process.env.OMNICELL_PASSWORD : '';
        const omnicellAlpha = process.env.OMNICELL_ALPHANAME ? process.env.OMNICELL_ALPHANAME : '';

        this.smsApi = new OmnicellClient(omnicellLogin, omnicellPass, omnicellAlpha);
    }

    public async start() {
        await this.bot.launch();
        this.bot.reactions();
    }

    public notification(message: string) {
        this.bot.notification(message);
    }

    public warning(message: string) {
        this.bot.warning(message);
    }

    public error(message: string) {
        this.bot.error(message);
    }

    public userNews(message: string) {
        this.bot.userNews(message);
    }

    public adminNews(message: string) {
        this.bot.adminNews(message);
    }

    public async sms(to: string[], message: string, from?: string) {
        try {
            const allowSms = await getConfigValue('sms_allow');
            if (allowSms) {
                this.smsApi.bulkSms(to, message);
                logger.info(`Sms sent to [${to.join(',')}]`);
                return;
            }
        } catch (err) {
            logger.error(`MailingsManager.sms err: ${err}`);
        }

        this.bot.notification(`Сообщение для: ${to.join(',')}
${from ? `Отправитель: ${from}` : ''}
--------
${message}
--------`);
    }
}