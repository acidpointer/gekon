export interface IMessangerBot {
    notification: (msg: string) => void;
    warning: (msg: string) => void;
    error: (msg: string) => void;
    userNews: (msg: string) => void;
    adminNews: (msg: string) => void;
}

export interface TgChanIds {
    notify: string;
    warn: string;
    error: string;
    userNews: string;
    adminNews: string;
}