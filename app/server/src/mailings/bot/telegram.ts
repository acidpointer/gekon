import { Telegraf } from 'telegraf';

import logger from '../../logger';

import { IMessangerBot, TgChanIds } from './types';

export class Tg implements IMessangerBot {
    private token: string;
    private tg: Telegraf;
    private chanIds: TgChanIds;


    private ready: boolean = false;

    constructor(token: string, chanIds: TgChanIds) {
        this.token = token;

        this.ready = !!token;

        this.chanIds = { ...chanIds };
        this.tg = new Telegraf(this.token);
    }

    public reactions() {
        if (!this.ready) {
            throw new Error('Telegram bot not ready! Check the token!');
        }

        this.tg.start((ctx) => {
            ctx.reply('Добро пожаловать!');
        });
    
        this.tg.help((ctx) => {
            ctx.reply('Отправь мне стикер, чтобы поздороваться')
        });
    
        this.tg.on('sticker', (ctx) => {
            ctx.reply('👍');
        });
    
        this.tg.hears('Привет', (ctx) => {
            ctx.reply('Привет!');
        });

        this.tg.on('channel_post', (ctx) => {
            ctx.reply(`Your chat id is: ${ctx.chat.id}`);
        });
    }

    public async launch() {
        if (!this.ready) {
            return;
        }
        await this.tg.launch();
    }

    

    private async sendMessage(message: string, chanId: string) {
        try {
            await this.tg.telegram.sendMessage(chanId, message);
        } catch (err) {
            logger.error(`telegram.sendMessage err: ${err}`);
        }
    }

    public notification(message: string) {
        if (!this.ready || !!!this.chanIds.notify) {
            logger.error('telegram.notification err: Telegraf not initialized or wrong channel id!')
            return;
        }
        this.sendMessage(message, this.chanIds.notify);
    }

    public warning(message: string) {
        if (!this.ready || !!!this.chanIds.warn) {
            logger.error('telegram.warning err: Telegraf not initialized or wrong channel id!')
            return;
        }
        this.sendMessage(message, this.chanIds.warn);
    }

    public error(message: string) {
        if (!this.ready || !!!this.chanIds.error) {
            logger.error('telegram.error err: Telegraf not initialized or wrong channel id!')
            return;
        }
        this.sendMessage(message, this.chanIds.error);
    }

    public userNews(message: string) {
        if (!this.ready || !!!this.chanIds.userNews) {
            logger.error('telegram.userNews err: Telegraf not initialized or wrong channel id!')
            return;
        }
        this.sendMessage(message, this.chanIds.userNews);
    }

    public adminNews(message: string) {
        if (!this.ready || !!!this.chanIds.adminNews) {
            logger.error('telegram.adminNews err: Telegraf not initialized or wrong channel id!')
            return;
        }
        this.sendMessage(message, this.chanIds.adminNews);
    }
}