export interface ISmsApi {
    bulkSms: (to: string[], message: string) => Promise<void>;
    singleSms: (to: string, message: string) => Promise<void>;
}

export interface IOmnicellResponce {
    state: {
        value: 'Accepted' | 'canceled' | 'Rejected' | 'Enroute'
        error?: string
    }
    id: number
    date: string
    execTime: number
}