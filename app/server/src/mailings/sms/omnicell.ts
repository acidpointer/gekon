/**
 * API client for omnicell.ua
 * docs: docs.omnicell.ua
 */

import axios from 'axios';
import { isValidPhoneNumber } from 'libphonenumber-js';

import { base64Encode } from '../../utils/crypto';

import { ISmsApi } from './types';

const apiUrl = 'https://api.omnicell.com.ua/ip2sms/';

export default class OmnicellClient implements ISmsApi{

    private login: string;
    private password: string;
    private alphaname: string;

    constructor(login: string, password: string, alphaname: string) {
        this.login = login;
        this.password = password;
        this.alphaname = alphaname;
    }

    private getValidPhones(phones: string[]): string[] {
        let correct = [];
        for (const p of phones) {
            if (isValidPhoneNumber(p)) {
                correct.push(p);
            }
        }
        return correct;
    }

    public async singleSms(to: string, message: string, validity: '+10 min' | '+20 min' | '+30 min' = '+10 min') {
        this.bulkSms([to], message, validity);
    }

    public async bulkSms(to: string[], message: string, validity: '+10 min' | '+20 min' | '+30 min' = '+10 min') {
        const phones = this.getValidPhones(to);

        if (!phones.length) {
            return;
        }

        const resp = await axios({
            method: 'POST',
            url: apiUrl,
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Accept': 'application/json',
                'Authorization': `Basic ${base64Encode(`${this.login}:${this.password}`)}`
            },
            data: {
                id: 'bulk',
                source: this.alphaname,
                validity,
                desc: 'Bulk sms from gekon.one',
                type: 'SMS',
                to: phones.map(o => ({ msisdn: o })),
                body: {
                    value: message
                }
            }
        });
        
        // TODO: remove debug
        console.log('SMS RESP', resp);
    }
}