import Q from 'q';

import { BvParams, Lang, NotificationChannel, SND, SVD, TerminalAction, TerminalProfile, TerminalState } from './rpc/schema/terminal_types';
import { scryptCompare } from './utils/crypto';

import TerminalModel from './models/terminal.model';
import CompanyModel from './models/company.model';

import { getTerminalPrograms } from './utils';
import logger from './logger';
import { GekonServer } from './app';

export default class TerminalHandler {

    #state: Map<string, TerminalState> = new Map();

    constructor() {
    }

    public setLang(terminalId: string, language: Lang) {
        const old = this.#state.get(terminalId);
        if (old) {
            logger.info(`[TERMINAL] Language for ${old.name} set to ${language}`);
            this.#state.set(terminalId, {
                ...old,
                language,
            });
        }
        return Q.resolve();
    }

    public topUp(terminalId: string, amount: number) {
        const old = this.#state.get(terminalId);
        if (old) {
            this.#state.set(terminalId, {
                ...old,
                action: TerminalAction.TOPUP,
                totalMoney: old.totalMoney + amount,
                currentMoney: old.currentMoney + amount,
            });
        }
    }

    public canSpam(terminalId: string, cmds: number[][]) {
        const old = this.#state.get(terminalId);
        if (old) {
            logger.info(`[TERMINAL] Canspam request for ${old.name}`);
            this.#state.set(terminalId, {
                ...old,
                action: TerminalAction.CANSPAM,
                canCmds: cmds,
            });
        }
    }

    public stop(terminalId: string) {
        const old = this.#state.get(terminalId);
        if (old) {
            logger.info(`[TERMINAL] Stopped ${old.name}`);
            this.#state.set(terminalId, {
                ...old,
                action: TerminalAction.STOP,
            });
        }
    }

    public start(terminalId: string) {
        const old = this.#state.get(terminalId);
        if (old) {
            logger.info(`[TERMINAL] Start ${old.name}`);
            this.#state.set(terminalId, {
                ...old,
                action: TerminalAction.START,
            });
        }
    }

    public restart(terminalId: string) {
        const old = this.#state.get(terminalId);
        if (old) {
            logger.info(`[TERMINAL] Restart ${old.name}`);
            this.#state.set(terminalId, {
                ...old,
                action: TerminalAction.RESTART,
            });
        }
    }

    public notification(terminalId: string, channel: NotificationChannel, message: string) {
        const state = this.#state.get(terminalId);
        if (state) {
            switch (channel) {
                case NotificationChannel.ERROR: {
                    GekonServer.instance.mailings.error(message);
                    break;
                }
                case NotificationChannel.WARNING: {
                    GekonServer.instance.mailings.warning(message);
                    break;
                }
                case NotificationChannel.NEWS_ADMIN: {
                    GekonServer.instance.mailings.adminNews(message);
                    break;
                }
                case NotificationChannel.NEWS_USER: {
                    GekonServer.instance.mailings.userNews(message);
                    break;
                }
            }
        }
        return Q.resolve();
    }

    public update(terminalId: string, callback?: (error: void, response: TerminalState) => void): void {
        const state = this.#state.get(terminalId);
        if (state) {
            callback(null, state);
        } else {
            callback(null, null);
        }
    }

    public async auth(name: string, password: string, callback?: (error: Error, result: TerminalProfile) => void) {
        try {
            const terminal = await TerminalModel.findOne({ name });
            if (terminal) {
                const company = await CompanyModel.findById(terminal.companyId);
                if (company) {
                    if (scryptCompare(terminal.password, password)) {
                        this.#state.delete(terminal.id);
                        this.#state.set(terminal.id, {
                            name: terminal.name,
                            enabled: terminal.enabled,
                            action: TerminalAction.NONE,
                            canCmds: [[]],
                            totalMoney: terminal.money,
                            currentMoney: 0,
                            language: Lang.UA,
                            payUrl: '',
                            bvParams: {
                                bvBanknots: terminal.bvBanknots,
                                bvCapacity: terminal.bvCapacity,
                                bvConnected: false,
                                bvStatus: false,
                            }
                        });
                        logger.info(`[TERMINAL] '${terminal.name}' authorized! Company: '${company.name}'`);
                        callback(null, {
                            id: terminal.id,
                            enabled: terminal.enabled,
                            language: terminal.lang,
                            administratorPhone: company.administratorPhone,
                        });
                        return;
                    }
                }
            }
        } catch (err) {
            logger.error(`[TERMINAL] Auth for ${name} error: ${err}`);
        }

        callback(null, {
            id: '-1',
            enabled: false,
            language: Lang.EN,
            administratorPhone: '',
        });
    }

    public ping(): Q.IPromise<void> {
        return Q.resolve();
    }

    public debit(terminalId: string, debit: number) {
        if (debit > 0) {
            const old = this.#state.get(terminalId);
            if (old) {
                const nCurr = old.currentMoney - debit;
                const nTotal = old.totalMoney - debit;

                const totalMoney = nTotal >= 0 ? nTotal : 0;
                const currentMoney = nCurr >= 0 ? nCurr : 0;
                this.#state.set(terminalId, {
                    ...old,
                    action: TerminalAction.DEBIT,
                    totalMoney,
                    currentMoney,
                });
                TerminalModel.updateOne({ _id: terminalId }, { money: totalMoney });
            }
        }
        return Q.resolve();
    }

    public async getPrograms(terminalId: string, callback?: (error: void, response: string) => void) {
        const state = this.#state.get(terminalId);
        const lang = state.language === Lang.EN ? 'en' : state.language === Lang.UA ? 'ua' : 'ru';

        const p = await getTerminalPrograms(terminalId, lang);
        callback(null, JSON.stringify(p));
    }

    public async getPayUrl(name: string, callback?: (error: void, response: string) => void) {
        const terminal = await TerminalModel.findOne({ name });
        if (terminal) {
            const pay = GekonServer.instance.pay;
            const token = await pay.createToken({ type: 'terminal', target: name, companyId: terminal.companyId }, name);
            callback(null, `https://gekon.one/pay/${token}`);
        } else {
            callback(null, `https://gekon.one/`);
        }
    }

    public async workTime(terminalId: string, seconds: number) {
        const old = this.#state.get(terminalId);
        if (old) {
            try {
                const terminal = await TerminalModel.findById(terminalId);
                if (terminal) {
                    const hours = (seconds - 3) / 60 / 60;
                    await TerminalModel.updateOne({ id: terminalId }, {
                        currentWorkHours: terminal.currentWorkHours + hours,
                        totalWorkHours: terminal.totalWorkHours + hours,
                    });
                }
            } catch (err) {
                logger.error(`[TERMINAL] Update work time for '${old.name}' error: ${err}`);
            }
        }
        return Q.resolve();
    }

    public async cashTopUp(terminalId: string, money: number, bvParams: BvParams) {
        const old = this.#state.get(terminalId);
        if (old && money > 0) {
            const totalMoney = old.totalMoney + money;
            const currentMoney = old.currentMoney + money;
            this.#state.set(terminalId, {
                ...old,
                action: TerminalAction.TOPUP,
                currentMoney,
                totalMoney,
                bvParams,
            });
            try {
                await TerminalModel.updateOne({ id: terminalId }, {
                    money: totalMoney,
                    bvBanknots: bvParams.bvBanknots,
                    bvCapacity: bvParams.bvCapacity,
                });
            } catch (err) {
                logger.error(`[TERMINAL] Update cash for '${old.name}' error: ${err}`);
            }
        }
        return Q.resolve();
    }

    public confirmAction(terminalId: string, action: TerminalAction) {
        const old = this.#state.get(terminalId);
        if (action === old.action) {
            this.#state.set(terminalId, { ...old, action: TerminalAction.NONE });
        }
        return Q.resolve();
    }

    public snd(terminalId: string, snd: SND) {
        const state = this.#state.get(terminalId);
        if (state) {
            GekonServer.instance.influx.snd(snd).catch(err => {
                logger.error(`[TELEMETRY] SND Error: ${err}`);
            });
        }
        return Q.resolve();
    }

    public svd(terminalId: string, svd: SVD) {
        const state = this.#state.get(terminalId);
        if (state) {
            GekonServer.instance.influx.svd(svd).catch(err => {
                logger.error(`[TELEMETRY] SVD Error: ${err}`);
            });
        }
        return Q.resolve();
    }

    public logout(terminalId: string) {
        const old = this.#state.get(terminalId);
        if (old) {
            logger.info(`[TERMINAL] ${old.name} logged out!`);
            this.#state.set(terminalId, {
                ...old,
                language: Lang.UA,
                currentMoney: 0,
            });
        }
        return Q.resolve();
    }

    public setBv(terminalId: string, bvParams: BvParams) {
        const old = this.#state.get(terminalId);
        if (old) {
            this.#state.set(terminalId, {
                ...old,
                bvParams
            });
        }
        return Q.resolve();
    }

    public restartAll() {
        const ids = this.#state.keys();
        for (const id of ids) {
            this.restart(id);
        }
    }

    //
    public get state(): Map<string, TerminalState> {
        return this.#state;
    }
}