import crypto from 'crypto';

// Remember! NEVER IMPLEMENT CRYPTOGRAPHY FROM SCRATCH!

// Fuck bcrypt! All my homies uses scrypt!
export const scryptHash = (toHash: string): Promise<string> => {
    return new Promise((resolve, reject) => {
        // 32-bytes salt
        const salt = crypto.randomBytes(32).toString('hex');

        crypto.scrypt(toHash, salt, 64, (err, derivedKey) => {
            if (err) reject(err);
            resolve(salt + ':' + derivedKey.toString('hex'));
        });
    });
}

export const scryptCompare = (hashed: string, raw: string): Promise<boolean> => {
    return new Promise((resolve, reject) => {
        const [salt, key] = hashed.split(':');

        const keyBuffer = Buffer.from(key, 'hex')
        crypto.scrypt(raw, salt, 64, (err, derivedKey) => {
            if (err) reject(err);
            resolve(crypto.timingSafeEqual(keyBuffer, derivedKey));
        });
    });
}

export const base64Encode = (input: string | Buffer): string => {
    return Buffer.from(input).toString('base64');
}

export const base64Decode = (input: string): string => {
    return Buffer.from(input, 'base64').toString();
}

export const sha1hex = (input: string): string => {
    const hash = crypto.createHash('sha1');
    const data = hash.update(input, 'utf-8');
    return data.digest('hex');
}

export const sha1b64 = (input: string): string => {
    const sha1 = crypto.createHash('sha1');
    sha1.update(input);
    return sha1.digest('base64');
}

export const rsaEncrypt = (toEncrypt: string, pubKey: string): string => {
    const buffer = Buffer.from(toEncrypt);
    const encrypted = crypto.publicEncrypt(pubKey, buffer);
    return encrypted.toString('base64');
};

export const rsaDecrypt = (toDecrypt: string, privKey: string): string => {
    const buffer = Buffer.from(toDecrypt, 'base64');
    const decrypted = crypto.privateDecrypt(privKey, buffer);
    return decrypted.toString('utf8');
};