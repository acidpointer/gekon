export const dynamicSort = (property: string, order: string) => {
    let sortOrder = order === 'ASC' ? 1 : -1;
    return (a: any, b: any) => {
        let result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

export const numArrToStr = (arr: number[]): string => {
    let s = '';
    for (const n of arr) {
        s = `${s}${n}`;
    }
    return s;
}

export const strToNumArr = (str: string): number[] => {
    let r = [];
    for (const s of str) {
        r.push(Number(s));
    }
    return r;
}

// Source: https://stackoverflow.com/a/7837725/308645
export const compareArrays = (arr1: Array<any>, arr2: Array<any>): boolean => {
    let i = arr1.length;
    while (i--) {
        if (arr1[i] !== arr2[i]) return false;
    }
    return true
}

export const getRandomArrayElement = (arr: any[]): any => {
    return arr[Math.floor(Math.random() * arr.length)];  
}