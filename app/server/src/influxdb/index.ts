import { InfluxDB, Point, WriteApi } from '@influxdata/influxdb-client';
import { Bucket, BucketsAPI, OrgsAPI } from '@influxdata/influxdb-client-apis';

import { SND, SVD } from '../rpc/schema/terminal_types';

export default class Influx {
    #influxDb: InfluxDB;
    #org: string;
    #orgsApi: OrgsAPI;
    #bucketsAPI: BucketsAPI;

    #bucketName = 'controllers';

    #initDone = false;

    constructor(org: string, url: string, token: string) {
        this.#influxDb = new InfluxDB({ url, token });
        this.#org = org;
        this.#orgsApi = new OrgsAPI(this.#influxDb);
        this.#bucketsAPI = new BucketsAPI(this.#influxDb);
    }

    private init(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.#initDone) {
                resolve();
                return;
            }
            this.#orgsApi.getOrgs({ org: this.#org }).then(orgs => {
                if (orgs && orgs.orgs && orgs.orgs.length) {
                    const orgID = orgs.orgs[0].id;
                    if (orgID) {
                        this.#bucketsAPI.getBuckets({ orgID, name: this.#bucketName }).then(buckets => {
                            if (buckets && buckets.buckets && buckets.buckets?.length) {
                                this.#initDone = true;
                                resolve();
                                return;
                            }
                            resolve();
                        }).catch(err => {
                            //reject(`bucketsAPI.getBuckets err: ${err}`)
                            this.#bucketsAPI.postBuckets({
                                body: {
                                    orgID, name: this.#bucketName, retentionRules: [{
                                        everySeconds: 0,
                                        type: 'expire',
                                    }]
                                }
                            }).then(bucket => {
                                if (bucket) {
                                    this.#initDone = true;
                                    resolve();
                                    return;
                                }
                                reject(`Bucket not created!`);
                            }).catch(err => reject(`bucketsAPI.postBuckets err: ${err}`));
                        });
                        return;
                    }
                    reject(`orgID not found!`);
                    return;
                }
                reject('orgsApi.getOrgs: orgs not found!');
            }).catch(err => reject(`orgsApi.getOrgs err: ${err}`));
        });
    }

    public async svd(params: SVD) {
        await this.init();
        const writeApi = this.#influxDb.getWriteApi(this.#org, this.#bucketName);
        writeApi.useDefaultTags({ type: 'svd', id: `svd_${params.id}` });

        const err_chtn = new Point('err_chtn').booleanField('value', params.err_chtn);
        const err_gvs = new Point('err_gvs').booleanField('value', params.err_gvs);
        const err_connect_snd = new Point('err_connect_snd').booleanField('value', params.err_connect_snd);
        const err_connect_terminal = new Point('err_connect_terminal').booleanField('value', params.err_connect_terminal);

        const timer_5sec_start = new Point('timer_5sec_start').booleanField('value', params.timer_5sec_start);
        const timer_180sec_stop = new Point('timer_180sec_stop').booleanField('value', params.timer_180sec_stop);
        const timer_180sec_start = new Point('timer_180sec_start').booleanField('value', params.timer_180sec_start);

        const chtn = new Point('chtn').booleanField('value', params.chtn);

        const block1 = new Point('block1').booleanField('value', params.block1);
        const block2 = new Point('block2').booleanField('value', params.block2);
        const block3 = new Point('block3').booleanField('value', params.block3);
        const block4 = new Point('block4').booleanField('value', params.block4);

        const dnd1 = new Point('dnd1').floatField('value', params.dnd1);

        const temp_gvs = new Point('temp_gvs').floatField('value', params.temp_gvs);
        const temp_pvd = new Point('temp_pvd').floatField('value', params.temp_pvd);

        const ekn1 = new Point('ekn1').booleanField('value', params.ekn1);
        const ekn2 = new Point('ekn2').booleanField('value', params.ekn2);
        const ekn3 = new Point('ekn3').booleanField('value', params.ekn3);

        const ekpn = new Point('ekpn').booleanField('value', params.ekpn);
        const ekvk = new Point('ekvk').booleanField('value', params.ekvk);

        const dozpn = new Point('dozpn').booleanField('value', params.dozpn);
        const dozvk = new Point('dozvk').booleanField('value', params.dozvk);

        const ekgvs = new Point('ekgvs').booleanField('value', params.ekgvs);

        const mode = new Point('mode').intField('value', params.mode);

        writeApi.writePoints([
            err_chtn,
            err_gvs,
            err_connect_snd,
            err_connect_terminal,

            timer_5sec_start,
            timer_180sec_start,
            timer_180sec_stop,

            chtn,

            block1,
            block2,
            block3,
            block4,

            dnd1,

            temp_gvs,
            temp_pvd,

            ekn1,
            ekn2,
            ekn3,

            ekpn,
            ekvk,

            dozpn,
            dozvk,

            ekgvs,

            mode
        ]);

        await writeApi.close();
    }

    /*
    Simple query example:

    from(bucket: "controllers")
        |> range(start: -1m)
        |> filter(fn: (r) => r._measurement == "dnd1")
        |> filter(fn: (r) => r.type == "snd")
    */

    public async snd(params: SND) {
        await this.init();
        const writeApi = this.#influxDb.getWriteApi(this.#org, this.#bucketName);
        writeApi.useDefaultTags({ type: 'snd', id: `snd_15` });
        const dnd1 = new Point('dnd1').floatField('value', params.dnd1);
        const dnd2 = new Point('dnd2').floatField('value', params.dnd2);
        const dnd3 = new Point('dnd3').floatField('value', params.dnd3);
        const dnd4 = new Point('dnd4').floatField('value', params.dnd4);

        const du1 = new Point('du1').floatField('value', params.du1);
        const du2 = new Point('du2').floatField('value', params.du2);
        const du3 = new Point('du3').floatField('value', params.du3);
        const du4 = new Point('du4').floatField('value', params.du4);

        const ddk = new Point('ddk').floatField('value', params.ddk);

        const temp = new Point('temp').floatField('value', params.temp);

        const fc_err1 = new Point('fc_err1').booleanField('value', params.fc_err1);
        const fc_err2 = new Point('fc_err2').booleanField('value', params.fc_err2);
        const fc_err3 = new Point('fc_err3').booleanField('value', params.fc_err3);

        const block1 = new Point('block1').booleanField('value', params.block1);
        const block2 = new Point('block2').booleanField('value', params.block2);
        const block3 = new Point('block3').booleanField('value', params.block3);
        const block4 = new Point('block4').booleanField('value', params.block4);

        const block11 = new Point('block11').booleanField('value', params.block11);
        const block22 = new Point('block22').booleanField('value', params.block22);
        const block33 = new Point('block33').booleanField('value', params.block33);
        const block44 = new Point('block44').booleanField('value', params.block44);

        const n1_timer = new Point('n1_timer').booleanField('value', params.n1_timer);
        const n2_timer = new Point('n2_timer').booleanField('value', params.n2_timer);
        const n3_timer = new Point('n3_timer').booleanField('value', params.n3_timer);
        const n4_timer = new Point('n4_timer').booleanField('value', params.n4_timer);

        const millis1 = new Point('millis1').intField('value', params.millis1);
        const millis2 = new Point('millis2').intField('value', params.millis2);
        const millis3 = new Point('millis3').intField('value', params.millis3);
        const millis4 = new Point('millis4').intField('value', params.millis4);

        const ekv = new Point('ekv').booleanField('value', params.ekv);

        writeApi.writePoints([
            dnd1, dnd2, dnd3, dnd4,
            du1, du2, du3, du4,

            ddk,
            temp,

            fc_err1, fc_err2, fc_err3,

            block1, block2, block3, block4,
            block11, block22, block33, block44,

            n1_timer, n2_timer, n3_timer, n4_timer,
            millis1, millis2, millis3, millis4,

            ekv,
        ]);

        await writeApi.close();
    }

}