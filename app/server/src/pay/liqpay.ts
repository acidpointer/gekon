import axios from 'axios';

import { base64Encode, sha1b64 } from '../utils/crypto';

export interface ApiParams {
    version: number;
    amount: number;
    currency: 'USD' | 'EUR' | 'RUB' | 'UAH' | 'BYN' | 'KZT';
    order_id: string;
    description: string;
    action: 'pay' | 'hold' | 'subscribe' | 'paydonate' | 'auth' | 'payqr';
}

export default class LiqPay {
    private host: string = 'https://www.liqpay.ua/api/';
    private privKey: string;
    private pubKey: string;

    private callbackUrl: string;

    constructor(privKey: string, pubKey: string, callbackUrl: string) {
        this.privKey = privKey;
        this.pubKey = pubKey;
        this.callbackUrl = callbackUrl;
    }

    public checkDataSign(data: string, signature: string): boolean {
        return sha1b64(this.privKey + data + this.privKey) === signature;
    }

    public getData(params: ApiParams): { data: string, signature: string } {
        const data = base64Encode(JSON.stringify({ ...params, public_key: this.pubKey, server_url: this.callbackUrl }));
        const signature = sha1b64(this.privKey + data + this.privKey);
        return { data, signature };
    }

    public async api(path: string, params: ApiParams): Promise<object> {
        const { data, signature } = this.getData(params);
        return axios.post(this.host + path, { data, signature }, {
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4495.0 Safari/537.36'
            }
        });
    }
}