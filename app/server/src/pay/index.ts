import { customAlphabet } from 'nanoid';

import redis from '../redis';

import LiqPay from './liqpay';
import { ILiqPayResponce } from './types';

import Payment from '../models/payment.model';
import PayOrder, { IPayOrder } from '../models/payorder.model';

import { base64Decode } from '../utils/crypto';

import logger from '../logger';

export interface ITarget {
    type: 'customer' | 'terminal';
    target: string;
    companyId: string;
}

export interface IOrderParams {
    amount: number;
    action: 'pay' | 'payqr';
    currency: 'USD' | 'EUR' | 'UAH';
    type: 'customer' | 'terminal';
    target: string;
    companyId: string;
}

export interface IOrder extends IOrderParams {
    orderId: string;
}

const rand = customAlphabet('WETYUIPXC012345789', 33);

export default class PayManager {
    private liqPay: LiqPay;

    private liqPayPrivKey: string;
    private liqPayPubKey: string;

    constructor(liqPayPrivKey: string, liqPayPubKey: string) {
        this.liqPayPrivKey = liqPayPrivKey;
        this.liqPayPubKey = liqPayPubKey;

        this.liqPay = new LiqPay(this.liqPayPrivKey, this.liqPayPubKey, 'https://gekon.one/liqpay');
    }

    public async createToken(target: ITarget, sourceTerminal: string, lifetime = 60 * 15): Promise<string> {
        const token = rand();

        await this.deleteOwnToken(sourceTerminal);

        await redis.set(`tokenown-${sourceTerminal}`, token);
        await redis.expire(`tokenown-${sourceTerminal}`, lifetime);

        await redis.set(`pay-${token}`, JSON.stringify(target));
        await redis.expire(`pay-${token}`, lifetime);

        
        return token;
    }

    public async deleteOwnToken(terminal: string) {
        const avail = await redis.get(`tokenown-${terminal}`);
        if (avail) {
            await redis.del(`pay-${avail}`);
            await redis.del(`tokenown-${terminal}`);
        }
    }

    public async getTokenTarget(token: string): Promise<ITarget> {
        const avail = await redis.get(`pay-${token}`);
        if (avail) {
            return JSON.parse(avail) as ITarget;
        }
        throw new Error('Wrong token!');
    }

    private async createTempOrder(params: IOrderParams, lifetime = 60 * 30): Promise<IOrder> {
        const orderId = rand();
        const order = { ...params, orderId };
        await redis.set(`order-${orderId}`, JSON.stringify(order));
        await redis.expire(`order-${orderId}`, lifetime);
        return order;
    }

    private async confirmOrder(orderId: string, payId: string): Promise<IPayOrder> {
        const raw = await redis.get(`order-${orderId}`);
        if (raw) {
            const orderTmp = JSON.parse(raw) as IOrder;
            const po = await PayOrder.create({
                ...orderTmp,
                payId,
            });
            return po._doc;
        }
        throw new Error(`Order ${orderId} not found!`);
    }

    public async getData(
        amount: number,
        type: 'customer' | 'terminal',
        target: string,
        companyId: string,
        token: string,
        action: 'pay' | 'payqr' = 'pay',
        currency: 'USD' | 'EUR' | 'UAH' = 'UAH'
    ): Promise<{ data: string, signature: string } | null> {

        const availToken = await redis.get(`pay-${token}`);
        if (!availToken) {
            throw new Error('Token not found, order could not be created!');
        }

        const order = await this.createTempOrder({
            amount,
            type,
            target,
            action,
            currency,
            companyId,
        });

        return this.liqPay.getData({
            action,
            currency,
            amount,
            description: `${target}`,
            order_id: order.orderId,
            version: 3,
        });
    }

    public async confirm(data: string, signature: string): Promise<IPayOrder> {
        const isValid = this.liqPay.checkDataSign(data, signature);
        if (!isValid) throw new Error('Data signature wrong!');

        const {
            amount,
            action,
            status,
            description,
            sender_phone,
            sender_card_mask2,
            sender_card_bank,
            transaction_id,
            order_id,
            currency
        } = (JSON.parse(base64Decode(data)) as ILiqPayResponce);

        const payment = await Payment.create({
            amount,
            action,
            status,
            description,
            sender_phone,
            sender_card_mask2,
            sender_card_bank,
            transaction_id,
            currency,
        });

        if (payment) {
            logger.info(`Payment accepted with payId: ${payment.payId} amount = ${payment.amount}`);
            const order = await this.confirmOrder(order_id, payment.payId);
            return order;
        } else {
            throw new Error('Payment creation failed!');
        }
    }
}