import fs from 'fs';
import pino from 'pino';
import { multistream, Streams, prettyStream, } from 'pino-multi-stream';

const logPath = process.env.LOGPATH ? process.env.LOGPATH : './logs/';
const logFile = `${logPath}/gekon-latest.log`;

if (!fs.existsSync(logPath)) {
    console.log('Logs will be stored in', logPath);
    fs.mkdirSync(logPath);
}


const streams: Streams = [
    {
        stream: prettyStream(
            {
                prettyPrint: {
                    colorize: true,
                    translateTime: "SYS:standard",
                    ignore: "hostname,pid" // add 'time' to remove timestamp
                },
            }
        )
    },
    {
        stream: prettyStream(
            {
                prettyPrint: {
                    translateTime: "SYS:standard",
                    ignore: "hostname,pid" // add 'time' to remove timestamp
                },
                dest: fs.createWriteStream(logFile),
            }
        )
    }
];

const logger: pino.Logger = pino({
    name: 'TERMINAL',
    level: 'debug',
}, multistream(streams));

export default logger;