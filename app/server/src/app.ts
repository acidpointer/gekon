import mongoose from 'mongoose';

import Server from './api/rest';

import {
    initWashtermDefaultPrograms,
    initDefaultConfiguration
} from './init-db';

import { RPCServer } from './rpc';
import MailingsManager from './mailings';
import PayManager from './pay';

import logger from './logger';
import { FastifyInstance } from 'fastify';
import TerminalHandler from './terminal';
import Influx from './influxdb';

export class GekonServer {

    static #instance: GekonServer;

    #host: string;
    #webPort: number;
    #rpcPort: number;

    #rpc: RPCServer;

    #terminalHandler: TerminalHandler;

    #mailingsManager: MailingsManager;
    #payManager: PayManager;
    #mongoose: mongoose.Mongoose;
    #influx: Influx;

    #server: FastifyInstance;

    private constructor(host: string, webPort: number, rpcPort: number) {
        this.#host = host;
        this.#webPort = webPort;
        this.#rpcPort = rpcPort;

        this.#terminalHandler = new TerminalHandler();
        this.#rpc = new RPCServer(this.#terminalHandler);
        this.#mailingsManager = new MailingsManager();
        this.#payManager = new PayManager('sandbox_ncfiKbF995RKNxez4OdIC728rs9Rw9M0zipi1MAE', 'sandbox_i87645624203');

        this.#server = Server;

        this.#influx = new Influx('gekon', 'http://influxdb:8086', process.env.INFLUXDB_ADMIN_TOKEN);

        this.#rpc.server.on('error', (err: Error) => {
            logger.error(`[RPC] Error: ${err}`);
        });

        process.on('SIGUSR2', async (_) => {
            logger.warn('--< [SIGUSR2] >--');

            this.#terminalHandler.restartAll();
        
            await this.#server?.close();
            logger.warn('[WEB] Closed!');
        
            await this.#rpc.stop();
            logger.warn('[RPC] Stopped!');
        
            logger.warn('--< [/SIGUSR2] >--');
            process.exit(0);
        });
    }

    private async connectDb() {
        const mongoUser = process.env.MONGO_USER;
        const mongoPass = process.env.MONGO_PASS;
        const url = `mongodb://${mongoUser}:${mongoPass}@mongo:27017/gekon`;
        this. #mongoose = await mongoose.connect(url, {
            autoIndex: false,
        });
    }

    public async start() {
        await this.connectDb();
        logger.info(`[APP] [${process.pid}] Connected to database!`);
        // Okay, we now use the programs constructor
        //await initWashtermDefaultPrograms();
        await initDefaultConfiguration();
        logger.info(`[APP] [${process.pid}] Database initialized!`);
        await this.#rpc.start(this.#host, this.#rpcPort);
        logger.info(`[APP] [${process.pid}] RPC Ready!`);
        await this.#server.listen(this.#webPort, this.#host);
        logger.info(`[APP] [${process.pid}] Web ready!`);
    }

    public static get instance(): GekonServer {
        if (!this.#instance) {
            this.#instance = new GekonServer('gekon', 7272, 9897);
        }
        return this.#instance;
    }

    public get pay(): PayManager {
        return this.#payManager;
    }

    public get mailings(): MailingsManager {
        return this.#mailingsManager;
    }

    public get influx(): Influx {
        return this.#influx;
    }

    public get handler(): TerminalHandler {
        return this.#terminalHandler;
    }
}

const app = GekonServer.instance;
app.start();