import Redis from 'ioredis';
import logger from './logger';

const redis = new Redis({
    host: 'redis',
    connectionName: 'gekon-redis',
    connectTimeout: 500,
    maxRetriesPerRequest: 1
});

redis.on('connect', () => { 
    logger.info('Connected to Redis server!');
});

redis.on('reconnecting', () => {
    logger.warn('Reconnecting to Redis server!');
});

redis.on('error', (err) => {
    logger.error(`Redis client error: ${err}`);
});

export default redis;
